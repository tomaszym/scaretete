
val kamonVersion = "0.6.1"

val dependencies = Seq(
  "org.scala-lang" % "scala-reflect" % "2.11.8",
  "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4",
  "com.typesafe.akka" %% "akka-actor" % "2.4.2",
  "com.typesafe.akka" %% "akka-slf4j" % "2.4.2",
  "com.typesafe.akka" %% "akka-testkit" % "2.4.2",
  "org.scalatest" %% "scalatest" % "2.2.4" % "test",
  "com.googlecode.kiama" %% "kiama" % "1.8.0",
  "com.softwaremill.macwire" %% "macros" % "1.0.5",
  "com.softwaremill.macwire" %% "runtime" % "1.0.5",
  "org.scalaz" %% "scalaz-core" % "7.2.0",
  "com.chuusai" %% "shapeless" % "2.3.0",
  "ch.qos.logback" % "logback-classic" % "1.1.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
  "org.scalamock" %% "scalamock-scalatest-support" % "3.2" % "test",
  "io.kamon" %% "kamon-core" % kamonVersion,
  "io.kamon" %% "kamon-statsd" % kamonVersion,
  "io.kamon" %% "kamon-scala" % kamonVersion,
  "io.kamon" %% "kamon-akka" % kamonVersion,
  "io.kamon" %% "kamon-autoweave" % kamonVersion,
  "com.github.wookietreiber" %% "scala-chart" % "latest.integration",
  "com.itextpdf" % "itextpdf" % "5.5.6"
)

lazy val root = (project in file(".")).settings(
    name := """scarete""",
    version := "1.0",
    scalaVersion := "2.11.8",
    scalacOptions in ThisBuild ++= Seq("-unchecked", "-deprecation", "-feature"),
    libraryDependencies ++= dependencies,
    coverageEnabled := false
).enablePlugins(JavaAppPackaging).enablePlugins(AspectJWeaver)