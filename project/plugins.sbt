
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.1.0-RC1")

addSbtPlugin("com.gilt.sbt" % "sbt-aspectjweaver" % "0.0.2")

addSbtPlugin("io.kamon" % "aspectj-runner" % "0.1.3")

addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.3.5")