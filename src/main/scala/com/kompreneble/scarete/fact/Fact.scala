package com.kompreneble.scarete.fact

case class PriceQueryId(stringId: String)
case class PriceQuery(id: PriceQueryId)

case class AttrKey(stringName: String) extends AnyVal {
  override def toString: String = stringName
}
case class FactType(stringType: String) extends AnyVal

abstract class Attr
case class AttrValue(v: Any) extends Attr {
  override def toString: String = v.toString
}
case class SubFact(tpe: FactType, attributes: Map[AttrKey, Attr]) extends Attr
case class Fact(tpe: FactType, attributes: Map[AttrKey, Attr], isQuery: Boolean = false){
  override def toString: String = (if(isQuery) "Query-" else "") + s"${tpe.stringType}(" + attributes.mkString(", ")+ ")"
}