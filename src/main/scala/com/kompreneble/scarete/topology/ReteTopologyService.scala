package com.kompreneble.scarete.topology

import com.kompreneble.scarete.condition._
import com.kompreneble.scarete.prf.ast._

trait ReteTopologyService {

  def buildTopologyFor(rules: List[PrfRule]): List[ProductionRule]
}

object FoldLeftReteTopologyService extends ReteTopologyService with AlphaChainTranslation {

  override def buildTopologyFor(rules: List[PrfRule]): List[ProductionRule] = {

    def mapConditions(conditions: List[PrfEntityConditions]): ConstraintTree = conditions.tail.foldLeft[ConstraintTree](
      alphaChainFromPrf(conditions.head)
    ){ case (tree, condition) =>
      ConstraintBranch(tree :: alphaChainFromPrf(condition) :: Nil)
    }

    rules.map { rule =>
      ProductionRule(mapConditions(rule.conditions), ProductionId(rule.effects.dummy))
    }
  }
}
