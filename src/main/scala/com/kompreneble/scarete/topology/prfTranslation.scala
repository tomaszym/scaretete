package com.kompreneble.scarete.topology

import com.kompreneble.scarete.condition._
import com.kompreneble.scarete.fact.FactType
import com.kompreneble.scarete.prf.ast._

trait AlphaChainTranslation extends PrfExprTranslation {

  def alphaChainFromPrf(prf: PrfEntityConditions): ConstraintChain = {

    val tpe = FactType(prf.tpe)

    val constraints: List[Constraint] = prf.conditions.map { prf =>

      val comparator = comparatorFromPrf(prf.op)

      Constraint(comparator, exprFromPrf(prf.lhs), exprFromPrf(prf.rhs))
    }
    ConstraintChain(tpe, constraints)
  }

  private def comparatorFromPrf(op: String): Comparator = op match {
    case "=" => Eq
    case "!=" => NEq
    case "<" => Lt
    case "=<" => LEt
    case ">" => Lt
    case "=>" => LEt
  }
}

trait PrfExprTranslation extends PrfSumTranslation {

  def exprFromPrf(prf: PrfExpr): Expr = prf match {
    case s: PrfSum => sumFromPrf(s)
    case p: PrfProd => prodFromPrf(p)
    case v: PrfValue => valFromPrf(v)
    case f: PrfFun => ???
  }
}

trait PrfSumTranslation extends PrfProdTranslation {

  def sumFromPrf(s: PrfSum): SumExpr =
    s.elems.map(sumElemFromPrf).foldLeft(SumExpr()) { case (xs, y) =>
      xs + y
    }

  private def sumElemFromPrf(e: PrfSumElem): SumExpr = e match {
    case PrfSumElem(PrfPlus, elem) => SumExpr(plus = List(sumOrProdOrValFromPrf(elem)))
    case PrfSumElem(PrfMinus, elem) => SumExpr(minus = List(sumOrProdOrValFromPrf(elem)))
  }

}

trait PrfProdTranslation extends PrfValTranslation { this: PrfSumTranslation =>

  def sumOrProdOrValFromPrf(prf: PrfExpr): Expr = prf match {
    case v: PrfValue => valFromPrf(v)
    case prod: PrfProd => prodFromPrf(prod)
    case sum: PrfSum => sumFromPrf(sum)
  }

  def prodFromPrf(p: PrfProd): ProdExpr =
    p.elems.map(prodElemFromPrf).foldLeft(ProdExpr()){ case (xs, y) =>
      xs * y
    }

  private def prodElemFromPrf(p: PrfProdElem): ProdExpr = p match {
    case PrfProdElem(PrfMul, elem) => ProdExpr(multiply = List(sumOrProdOrValFromPrf(elem)))
    case PrfProdElem(PrfDiv, elem) => ProdExpr(divide = List(sumOrProdOrValFromPrf(elem)))
  }
}

trait PrfValTranslation {

  def valFromPrf(v: PrfValue): Expr = v match {
    case PrfAttribute(attr) => AttrExpr(attr)
    case PrfIntLiteral(i) => ValExpr(i)
    case PrfDecimalLiteral(d) => ValExpr(d)
    case PrfStringLiteral(s) => ValExpr(s)
    case PrfBinding(b) => VariableBindingExpr(b)
  }

}