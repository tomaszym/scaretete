package com.kompreneble.scarete.encoder

import com.kompreneble.scarete.fact._
import shapeless.labelled._
import shapeless._
import syntax.typeable._

object AttrEncoder {
  abstract class AttrEncoder[T] {
    def encode(t: T): Attr
  }
  implicit def stringAttr: AttrEncoder[String] = new AttrEncoder[String] {
    override def encode(t: String): Attr = AttrValue(t)
  }

  implicit def doubleAttr: AttrEncoder[Double] = new AttrEncoder[Double] {
    override def encode(t: Double): Attr = AttrValue(t)
  }

  implicit def intAttr: AttrEncoder[Int] = new AttrEncoder[Int] {
    override def encode(t: Int): Attr = AttrValue(t)
  }

  abstract class WrappedAttrEncoder[Wrapped, SubRepr](implicit tpe: Typeable[Wrapped]) {
    def encode(v: SubRepr): Attr
  }

  implicit def hNilFormat[Wrapped](
                                    implicit t: Typeable[Wrapped]
                                  ): WrappedAttrEncoder[Wrapped, HNil] = new WrappedAttrEncoder[Wrapped, HNil] {
    def encode(n: HNil) = SubFact(FactType(t.describe), Map())
  }

  implicit def hListFormat[Wrapped, Key <: Symbol, Value, Remaining <: HList](
                                                                               implicit
                                                                               t: Typeable[Wrapped],
                                                                               key: Witness.Aux[Key],
                                                                               headEncoder: Lazy[AttrEncoder[Value]], // svc doesn't need to be a RootJsonFormat
                                                                               tailEncoder: WrappedAttrEncoder[Wrapped, Remaining]
                                                                             ): WrappedAttrEncoder[Wrapped, FieldType[Key, Value] :: Remaining] =
    new WrappedAttrEncoder[Wrapped, FieldType[Key, Value] :: Remaining] {
      def encode(ft: FieldType[Key, Value] :: Remaining): Attr =
        tailEncoder.encode(ft.tail) match {
          case s@SubFact(tpe, attrs) =>
            val k: AttrKey = AttrKey(key.value.name)
            val v: Attr = headEncoder.value.encode(ft.head)
            SubFact(tpe, attrs.+((k, v)))

          case AttrValue(_) => throw new Exception("asf")
        }
    }

  implicit def familyFormat[T, Repr](
                                      implicit
                                      gen: LabelledGeneric.Aux[T, Repr],
                                      sg: Cached[Strict[WrappedAttrEncoder[T, Repr]]],
                                      tpe: Typeable[T]
                                    ): AttrEncoder[T] = new AttrEncoder[T] {
    def encode(t: T): Attr = sg.value.value.encode(gen.to(t))
  }

}
