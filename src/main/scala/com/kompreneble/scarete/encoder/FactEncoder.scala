package com.kompreneble.scarete.encoder

import com.kompreneble.scarete.encoder.AttrEncoder.AttrEncoder
import com.kompreneble.scarete.fact._
import shapeless._
import syntax.typeable._


abstract class RootFactEncoder[T] {
  def encode(t: T): Fact
}


trait FactEncoder {
  import AttrEncoder._

  implicit def rootEncoder[T](
                               implicit
                               attrEncoder: Cached[Strict[AttrEncoder[T]]],
                               evidence: LabelledGeneric[T]
                             ): RootFactEncoder[T] = new RootFactEncoder[T] {
    override def encode(t: T): Fact = attrEncoder.value.value.encode(t) match {
      case SubFact(tpe, attrs) =>
        Fact(tpe, attrs, isQuery = false)
      case AttrValue(_) => throw new Exception("not possible thanks to an evidence")
    }
  }

  implicit def encode[T: RootFactEncoder](t: T): Fact = {
    implicitly[RootFactEncoder[T]].encode(t)
  }


}
