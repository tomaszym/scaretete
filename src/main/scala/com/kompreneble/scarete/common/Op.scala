package com.kompreneble.scarete.common

/** Operator
  *
  * @param stringOperator
  */
case class Op(stringOperator: String) extends AnyVal
