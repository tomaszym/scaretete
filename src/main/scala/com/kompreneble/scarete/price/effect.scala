package com.kompreneble.scarete.price

sealed trait PriceRuleEffect

case class AbsoluteChange(amount: Double) extends PriceRuleEffect
case class PercentageChange(percent: Double) extends PriceRuleEffect


// for now doesn't matter
//case class SetFixedDistancePrice(prices: List[PriceForDistanceRange]) extends PriceRuleEffect
//case class SetFixedHopPrice(prices: List[PriceForBusHop]) extends PriceRuleEffect


