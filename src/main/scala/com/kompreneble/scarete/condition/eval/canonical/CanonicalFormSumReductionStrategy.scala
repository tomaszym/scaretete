package com.kompreneble.scarete.condition.eval.canonical

import com.kompreneble.scarete.condition._
import com.kompreneble.scarete.condition.eval.EvalUtil
import com.kompreneble.scarete.condition.eval.normal.NormalRules
import com.typesafe.scalalogging.StrictLogging
import org.kiama.rewriting.Rewriter._
import org.kiama.rewriting.Strategy
import org.kiama.util.OutputEmitter

import scala.collection.Set

trait CanonicalFormSumReductionStrategy extends NormalRules with EvalUtil with StrictLogging {
  implicit val sumExprSemiGroup = new scalaz.Semigroup[SumExpr] {
    override def append(f1: SumExpr, f2: => SumExpr): SumExpr = SumExpr(f1.plus ++ f2.plus, f1.minus ++ f2.minus)
  }

//  object SumReduction {
//    def unapply(expr: Expr): Option[SumExpr] = sumReductionStrategy(expr) match {
//      case None => None
//      case Some(x: SumExpr) => Some(x)
//      case els: Any => throw new IllegalStateException(s"Found $els as the input of SumReduction. (SumExpr expected)")
//    }
//  }

  val sumReductionStrategy: Strategy = log(rule[Expr] {

    /** If there is anything to sum (at least two elems with the same SummableType)
      */
    case SumExpr(plus, minus) if (plus++minus).groupBy(_.summableType).exists{case (_,exprs) => exprs.length >= 2 } =>

      val plusGrouped = plus.groupBy(_.summableType).mapValues { exprs =>
          SumExpr(plus = exprs)
      }
      val minusGrouped = minus.groupBy(_.summableType).mapValues { exprs =>
        SumExpr(minus = exprs)
      }

      import scalaz.Scalaz._

      val typeMap: Map[SummableType, SumExpr] = plusGrouped |+| minusGrouped

      val result = typeMap.map {

        // numbers
        case (SummableType(m, d), sum) if m.isEmpty && d.isEmpty =>
          sum.applyStrategy(normalizeStrategy)

        case (typ, sum) =>
          val allProds: List[ProdExpr] = (sum.plus++sum.minus).map(wrapNonProdInProdTimesOne)
//          assert(allProds.forall(_.divide.isEmpty),"div not expected here")

          logger.debug(s"allProds: $allProds")
          /* Common part */
          val intersectMul: List[Expr] = allProds.map(_.multiply).foldLeft(allProds.head.multiply) {case (a,b) => a.intersect(b)}
          val intersectDiv: List[Expr] = allProds.map(_.divide).foldLeft(allProds.head.divide) {case (a,b) => a.intersect(b)}

          val commonPart = ProdExpr(intersectMul, intersectDiv)

          val plus = sum.plus.map(wrapNonProdInProdTimesOne).map { prod =>
              prod./(commonPart)
          }
//          logger.debug(s"plus: $plus")
          val minus = sum.minus.map(wrapNonProdInProdTimesOne).map { prod =>
            prod./(commonPart)
          }
//          logger.debug(s"minus: $minus")

//          val reduced: Expr = reduceSums(SumExpr(plus,minus))
          val reduced: Expr = SumExpr(plus,minus).applyStrategy(normalizeStrategy)

          ProdExpr(reduced :: intersectMul)
        }.toList
        SumExpr(result)
  },"s-red->  ", em)


  case class SummableType(mult: Set[String], div: Set[String])
  object SummableType {
    def apply(s: String): SummableType = SummableType(Set(s), Set())
    def empty = SummableType(Set(),Set())
  }

  implicit class NeutralFunctionZero(expr: Expr) {
    private def simpleSummableType(expr: Expr): Option[String] = expr match {
      case ValExpr(_, _) => None
      case VariableBindingExpr(v) => Some("var-"+v)
      case els: Expr =>
        throw new IllegalArgumentException("Before this step the expression has to " +
          "be in correct form. "+ els + "(" + els.getClass.getName + ") not allowed.")
    }

    def summableType: SummableType = {
      expr match {
        case v: ValExpr => SummableType.empty
        case VariableBindingExpr(v) => SummableType("var-" + v)
        case ProdExpr(mult, div) => SummableType(
          mult = mult.flatMap(simpleSummableType).toSet,
          div  = div.flatMap(simpleSummableType).toSet
        )
      }
    }
  }

  /** x = x * 1 so (x) could be pulled out and (1) summed.
    *
    * consider case x*x
    */
  def wrapNonProdInProdTimesOne(expr: Expr): ProdExpr = expr match {
    case prod: ProdExpr => prod
    case els: Expr => ProdExpr(els::ValExpr(1)::Nil)
  }

  /** Sum-reduces list of expressions, which are equal in terms of [[SummableType]],
    */
//  def sumOfEqualType(exprs: List[Expr]) = {
//    // those have common elems
//    val prods: List[ProdExpr] = exprs.map(wrapNonProdInProdTimesOne)
//
//    assert(prods.forall(_.divide.isEmpty))
//
//    prods match {
//        // trivial case, nothing to reduce
//      case head :: Nil => head
//        // more than one prods, need to reduce
//      case head :: tail =>
//        //Common stuff
//        val exprUnion: List[Expr] = tail.map(_.multiply).foldLeft(prods.head.multiply) { case (a,b) =>
//            a.union(b)
//        }
//        // the rest which is summable
//        val summableRest: List[List[Expr]] = prods.map(_.multiply.diff(exprUnion))
//
//    }
//  }

  def reduceSums(s: SumExpr): Expr = {


    import ValExpr._
    def sumVals(a: Expr, b: Expr): ValExpr = (a,b) match {
      case (ValExpr(IntegerType, x: Long), ValExpr(IntegerType, y: Long)) => ValExpr(IntegerType, x+y)
      case (ValExpr(DecimalType, x: Double), ValExpr(DecimalType, y: Double)) => ValExpr(DecimalType, x+y)
      case (ValExpr(IntegerType, x: Long), ValExpr(DecimalType, y: Double)) => ValExpr(DecimalType, x.toDouble+y)
      case (ValExpr(DecimalType, x: Double), ValExpr(IntegerType, y: Long)) => ValExpr(DecimalType, x+y.toDouble)
      case els: (Expr, Expr) => throw new IllegalArgumentException("This function handles only basic cases: summing of IntegerType or DecimalType or mixed. Got: " + a + " + " + b)
    }
    def minusVals(a: Expr, b: Expr) = (a,b) match {
      case (ValExpr(IntegerType, x: Long), ValExpr(IntegerType, y: Long)) => ValExpr(IntegerType, x-y)
      case (ValExpr(DecimalType, x: Double), ValExpr(DecimalType, y: Double)) => ValExpr(DecimalType, x-y)
      case (ValExpr(IntegerType, x: Long), ValExpr(DecimalType, y: Double)) => ValExpr(DecimalType, x.toDouble-y)
      case (ValExpr(DecimalType, x: Double), ValExpr(IntegerType, y: Long)) => ValExpr(DecimalType, x-y.toDouble)
      case els: (Expr, Expr) => throw new IllegalArgumentException("This function handles only basic cases: summing of IntegerType or DecimalType or mixed. Got: " + a + " - " + b)
    }

    val plused: ValExpr = s.plus.foldLeft(ValExpr(0))(sumVals)
    val minused = s.minus.foldLeft(plused)(minusVals)
    minused
  }



}
