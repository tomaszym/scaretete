package com.kompreneble.scarete.condition.eval.normal

import akka.actor.FSM.->
import com.kompreneble.scarete.condition._
import com.kompreneble.scarete.condition.eval.EvalUtil
import org.kiama.==>
import org.kiama.rewriting.Rewriter._
import org.kiama.rewriting.Strategy

trait NormalRules extends EvalUtil {

  def normalizeStrategy: Strategy = log(somebu(sumStrategy), "sum-->   ", em) <+ log(somebu(prodToProdStrategy), "prodP-->  ", em) <+ log(somebu(prodToExprStrategy), "prodE-->  ", em)

  protected val sumStrategy = rule[Expr] {

    /** Single Expr sums:
      */
    case SumExpr(expr :: Nil, Nil) => expr
    case SumExpr(Nil, IntegerValExpr(i)::Nil) => ValExpr(-i)
    case SumExpr(Nil, DecimalValExpr(i)::Nil) => ValExpr(-i)


    /** Filter zeros
      */
    case SumExpr(plus, minus) if plus.containsZeros => SumExpr(plus.filterNonZeros,minus)
    case SumExpr(plus, minus) if minus.containsZeros => SumExpr(plus,minus.filterNonZeros)

    /** Reduce nested sums
      */
    case SumExpr(plus,minus) if (plus++minus).exists(_.isInstanceOf[SumExpr]) =>
      val positiveSubSums: List[SumExpr] = plus.collect { case s: SumExpr => s }
      val negativeSubSums: List[SumExpr] = minus.collect { case s: SumExpr => s }

      SumExpr(
        plus = plus.diff(positiveSubSums) ++ positiveSubSums.flatMap(_.plus) ++ negativeSubSums.flatMap(_.minus),
        minus = minus.diff(negativeSubSums) ++ negativeSubSums.flatMap(_.plus) ++ positiveSubSums.flatMap(_.minus)
      )

    /** Reduce numbers in sums
      */
    case SumExpr(plus, minus) if (plus++minus).containsAtLeast2Numerics =>
      val (positiveNumbers, positiveNotNumbers) = plus.partitionNumbersNotNumbers
      val (negativeNumbers, negativeNotNumbers) = minus.partitionNumbersNotNumbers

      val negativeNegatedNumbers = negativeNumbers.map {
        case IntegerValExpr(i) => ValExpr(-i)
        case DecimalValExpr(i) => ValExpr(-i)
        case els: ValExpr => throw new IllegalArgumentException(s"Expected Integer or Decimal, got: $els")
      }

      val reducedPositive: ValExpr = (positiveNumbers++negativeNegatedNumbers).foldLeftNumbers

      SumExpr(reducedPositive :: positiveNotNumbers, negativeNotNumbers)
  }


  protected val prodToProdStrategy: Strategy = rule[ProdExpr] {

    /** Filter ones
      */
    case ProdExpr(m, d) if m.containsOnes => ProdExpr(m.filterNonOnes, d)
    case ProdExpr(m, d) if d.containsOnes => ProdExpr(m, d.filterNonOnes)


    /** Reduce nested prods
      */
    case ProdExpr(mul, div) if (mul++div).exists(_.isInstanceOf[ProdExpr]) =>
      val mulSubProds: List[ProdExpr] = mul.collect { case s: ProdExpr => s }
      val divSubProds: List[ProdExpr] = div.collect { case s: ProdExpr => s }

      ProdExpr(
        multiply = mul.diff(mulSubProds) ++ mulSubProds.flatMap(_.multiply) ++ divSubProds.flatMap(_.divide),
        divide = mul.diff(divSubProds) ++ divSubProds.flatMap(_.multiply) ++ mulSubProds.flatMap(_.divide)
      )

    /** Filter same nominator and denominator
      *
      */
    case ProdExpr(m, d) if m.intersect(d).nonEmpty =>
      val commonPart = m.intersect(d)
//      println(s"commonPart: $commonPart")
      ProdExpr(m.diff(commonPart), d.diff(commonPart))

  }


  protected val prodToExprStrategy: Strategy = rule[Expr]{

    /** Empty prod is just one
      */

    case ProdExpr(Nil, Nil) => ValExpr(1)
    /** Single Expr prod
      */
    case ProdExpr(expr::Nil, Nil) => expr
    case ProdExpr(Nil, IntegerValExpr(i)::Nil) => ValExpr(1d/i)
    case ProdExpr(Nil, DecimalValExpr(i)::Nil) => ValExpr(1d/i)


    /** Filter same nominator and denominator reduced to one ( eg $x/$x -> 1i)
      *
      */
    case ProdExpr(x::Nil, y::Nil) if x == y => ValExpr(1)

    /** Anything times zero is a zero
      */
    case ProdExpr(m, d) if m.containsZeros => ValExpr(0)

    /** Reduce numbers in sums
      */
    case ProdExpr(mul, div) if (mul ++ div).containsAtLeast2Numerics =>
      val (multiplyNumbers, multiplyNotNumbers) = mul.partitionNumbersNotNumbers
      val (divideNumbers, divideNotNumbers) = div.partitionNumbersNotNumbers

      /* x / 5 = x * 0.2 */
      val flipped = divideNumbers.map {
        case IntegerValExpr(i) => ValExpr(1d/i)
        case DecimalValExpr(i) => ValExpr(1d/i)
        case els: ValExpr => throw new IllegalArgumentException(s"Expected Integer or Decimal, got: $els")
      }

      val reducedMultiply: ValExpr = (multiplyNumbers++flipped).foldLeftNumbers

      SumExpr(reducedMultiply :: multiplyNotNumbers, divideNotNumbers)
  }


  /** A lot of helper functions to move there complexity of kiama strategies
    */
  implicit class RichExprList(l: List[Expr]) {

    def collectFirstVariableBinding: Option[VariableBindingExpr] = l.collectFirst {
      case v: VariableBindingExpr => v
    }

    /** Helper predicate to generate more helper functions
      */
    private def equalsPredicate(x: Long): PartialFunction[Expr,Boolean] = {
      case IntegerValExpr(i) => i == x
      case DecimalValExpr(d) => d == x.toDouble
      case els: Expr => false
    }

    private val isZeroPredicate: PartialFunction[Expr, Boolean] = equalsPredicate(0l)
    private val isOnePredicate: PartialFunction[Expr, Boolean] = equalsPredicate(1l)

    def containsZeros = l.exists(isZeroPredicate)
    def filterNonZeros = l.filterNot(isZeroPredicate)

    def containsOnes = l.exists(isOnePredicate)
    def filterNonOnes = l.filterNot(isOnePredicate)

    private val isNumericPredicate: PartialFunction[Expr, Boolean] = {
      case IntegerValExpr(_) => true
      case DecimalValExpr(_) => true
      case els: Expr => false
    }
    def allNumeric = l.forall(isNumericPredicate)
    def containsAtLeast2Numerics = l.count(isNumericPredicate) >= 2
    def filterNumeric: List[ValExpr] = l.filter(isNumericPredicate).map(_.asInstanceOf[ValExpr])

    def partitionNumbersNotNumbers: (List[ValExpr], List[Expr]) = l.partition(isNumericPredicate) match {
      case (nums, notNums) => (nums.map(_.asInstanceOf[ValExpr]), notNums)
    }

    def foldLeftNumbers = l.filterNumeric.foldLeft(ValExpr(0)){
      case (IntegerValExpr(i), IntegerValExpr(j)) => ValExpr(i+j)
      case (DecimalValExpr(i), IntegerValExpr(j)) => ValExpr(i+j)
      case (IntegerValExpr(i), DecimalValExpr(j)) => ValExpr(i+j)
      case (DecimalValExpr(i), DecimalValExpr(j)) => ValExpr(i+j)
      case els => throw new IllegalStateException(s"Expected to be called only on numerical ValExpr lists, got $els")
    }
  }



}
