package com.kompreneble.scarete.condition.eval.canonical

import com.kompreneble.scarete.condition._
import org.kiama.rewriting.Rewriter._
import org.kiama.rewriting.Strategy

/** Stuff related to transforming polynomials to
  * canonical form.
  *
  * Stages:
  *
  * 1. Move above fraction
  * 2. Multiply polynomials (multiply/divide)
  * 3. Sum (reduction)
  */
trait CanonicalPolynomialRules extends CanonicalFormPatterns with CanonicalFormSumReductionStrategy {

  val canonicalizationTopStrategy = log(rule[Expr] {

    case Relations(MoveAboveFraction(transformed) :: tail) => Relations(transformed :: tail)

    // (5+10)(2*2*2*2) -> 5*2*2*2*2 + 10*2*2*2*2
    case Relations(Constraint(Eq, SumBeforeParens(sum, prod), rhs) :: tail) =>
      val lhs = SumExpr(
        sum.plus.map(x => prod * x),
        sum.minus.map(x => prod * x)
      )
      Relations(Constraint(Eq, lhs, rhs) :: tail)

    // (5+10)(2*2*2*2) -> 5*2*2*2*2 + 10*2*2*2*2
    case Relations(Constraint(Eq, lhs, SumBeforeParens(sum, prod)) :: tail) =>
      val rhs = SumExpr(
        sum.plus.map(x => prod * x),
        sum.minus.map(x => prod * x)
      )
      Relations(Constraint(Eq, lhs, rhs) :: tail)

    case Relations(BindingsToLHS(transformed) :: tail) => Relations(transformed :: tail)
  },"c-ruls-> ", em)

  val canonicalizationStrategy: Strategy = canonicalizationTopStrategy <+ bottomup(sumReductionStrategy)

}
