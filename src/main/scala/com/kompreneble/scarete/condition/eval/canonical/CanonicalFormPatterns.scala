package com.kompreneble.scarete.condition.eval.canonical

import com.kompreneble.scarete.condition._
import com.kompreneble.scarete.condition.eval.EvalUtil

trait CanonicalFormPatterns extends EvalUtil {

  object MoveAboveFraction {
    def unapply(comp: Constraint): Option[Constraint] = {
      comp match {

        case Constraint(Eq, ProdExpr(_, Nil), ProdExpr(_, Nil)) => None

          // at least one div nonEmpty
        case Constraint(Eq, ProdExpr(lhsMult, nonEmptyLhsDiv), ProdExpr(rhsMult, nonEmptyRhsDiv)) =>
          Some(Constraint(Eq, ProdExpr(lhsMult++nonEmptyRhsDiv, Nil), ProdExpr(rhsMult ++ nonEmptyLhsDiv, Nil)))

          // div lhs. eg: x/5 = y -> x = 5y
        case Constraint(Eq, ProdExpr(lhsMult, Nil), rhs: Expr) => None
        case Constraint(Eq, ProdExpr(lhsMult, nonEmptyLhsDiv), rhs: Expr) =>
          Some(Constraint(Eq, ProdExpr(lhsMult, Nil), ProdExpr( rhs :: nonEmptyLhsDiv, Nil)))

          // x = y/6 -> 6x = y
        case Constraint(Eq, lhs, ProdExpr(rhsMult, Nil)) => None
        case Constraint(Eq, lhs, ProdExpr(rhsMult, nonEmptyRhsDiv)) =>
          Some(Constraint(Eq, ProdExpr(lhs :: nonEmptyRhsDiv), ProdExpr(rhsMult)))
        case els: Constraint => None
      }
    }
  }

  object SumBeforeParens {
    /** IF there is a sum above fraction separate it out
      *
      * @param expr
      * @return a tuple (separated sum, prod without this sum)
      */
    def unapply(expr: Expr): Option[(SumExpr, ProdExpr)] = {

      expr match {
        case prod: ProdExpr =>
          prod.multiply.collectFirst {
            case s: SumExpr => s
          }.map { sum =>
            val prodRest = prod.copy(multiply = prod.multiply.diff(Seq(sum)))
            (sum, prodRest)
          }
        case els: Expr => None

      }
    }
  }

  object BindingsToLHS {
    def unapply(arg: Constraint): Option[Constraint] =  arg match {
      case Constraint(Eq, lhs, rhs) if rhs.freeVariables.nonEmpty =>
        (lhs, rhs) match {
          case (lhsSum: SumExpr, rhsSum: SumExpr) => Some(Constraint(Eq, SumExpr(lhsSum.plus ++ rhsSum.minus, lhsSum.minus++rhsSum.plus), ValExpr(0)))
          case (lhsSum: SumExpr, rhs: Expr) => Some(Constraint(Eq, SumExpr(lhsSum.plus,rhs :: lhsSum.minus), ValExpr(0)))
          case (lhs: Expr, rhsSum: SumExpr) => Some(Constraint(Eq, SumExpr(lhs :: rhsSum.minus,rhsSum.plus), ValExpr(0)))
          case (lhsNotSum, rhsNotSum) => Some(Constraint(Eq, SumExpr(lhs::Nil,rhs::Nil), ValExpr(0)))
        }
      case els: Constraint => None
    }

  }
}
