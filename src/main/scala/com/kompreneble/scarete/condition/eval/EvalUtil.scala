package com.kompreneble.scarete.condition.eval

import com.kompreneble.scarete.condition.EvalService.EvalException
import com.kompreneble.scarete.condition._
import com.typesafe.scalalogging.{LazyLogging, StrictLogging}
import org.kiama.==>
import org.kiama.rewriting.Strategy
import org.kiama.util.{OutputEmitter, StringEmitter}

trait EvalUtil extends StrictLogging {

  /** Free variables
    */
  def freeVariables(t: Expr): Set[VariableBindingExpr] = {
    t match {
      case v: ValExpr => Set()
      case v: VariableBindingExpr => Set(v)
      case a: AttrExpr => throw EvalException(a)
      case Sub(m, x, n) => (freeVariables(m) - x) ++ freeVariables(n)
      case ProdExpr(m, d) => m.flatMap(freeVariables).toSet ++ d.flatMap(freeVariables).toSet
      case SumExpr(pos, neg) => pos.flatMap(freeVariables).toSet ++ neg.flatMap(freeVariables).toSet
      case Relations(cs) => cs.flatMap(c => c.lhs :: c.rhs :: Nil).flatMap(freeVariables).toSet
    }
  }

  def isNumericExpr(t: Expr): Boolean = {
    t match {
      case IntegerValExpr(_) => true
      case DecimalValExpr(_) => true
      case els: ValExpr => false
      case v: VariableBindingExpr => false
      case a: AttrExpr => throw EvalException(a)
      case Sub(m, x, n) => isNumericExpr(m) || isNumericExpr(n)
      case ProdExpr(m, d) => true
      case SumExpr(pos, neg) => true
      case Relations(cs) => cs.flatMap(c => c.lhs :: c.rhs :: Nil).exists(isNumericExpr)
    }
  }



  val em = new StringEmitter

  import math.max

  private def optionMaxFromList(xs: List[Option[Int]]): Option[Int] = {
    if(xs.isEmpty) None
    else Some(xs.flatten.max)
  }

  private def optionMax(i: Option[Int], j: Option[Int]): Option[Int] = {
    (i,j) match {
      case (None, None) => None
      case (Some(x), None) => Some(x)
      case (None, Some(y)) => Some(y)
      case (Some(x), Some(y)) => Some(max(x,y))
    }
  }

//  def maxLevel(e: Expr): Option[Int] = e match {
//    case Comparison(_, lhs, rhs) => optionMax(maxLevel(lhs), maxLevel(rhs)).map(_ + 1)
//    case SumExpr(xs, ys) =>
//      optionMax(
//        optionMaxFromList(xs.map(maxLevel)),
//        optionMaxFromList(xs.map(maxLevel))
//      ).map(_ + 1)
//
//
//    case ProdExpr(xs, ys) =>
//      optionMax(
//        optionMaxFromList(xs.map(maxLevel)),
//        optionMaxFromList(xs.map(maxLevel))
//      ).map(_ + 1)
//
//    case AttrExpr(_) => None
//    case ValExpr(_) => None
//    case VariableBindingExpr(_) =>
//    //    case ProdExpr(xs, ys) => max(xs.map(maxLevel).max, ys.map(maxLevel).max) + 1
////    case
//
//  }

  implicit class RichExpr(expr: Expr) {

    def freeVariables = KiamaEvalService.freeVariables(expr)
    def isNumericExpr = KiamaEvalService.isNumericExpr(expr)

    def applyStrategy(s: Strategy): Expr = s(expr) match{
      case Some(transformed: Expr) => transformed
      case None => expr // no rule applied, nothing happend
    }
  }

}
