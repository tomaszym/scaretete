//import com.kompreneble.scarete.condition.{Expr, LongValExpr, SumExpr, VariableBindingExpr}
//import org.kiama.rewriting.Rewriter._
//import org.kiama.util.OutputEmitter
//
//val flatten_red = rule[Expr] {
//  // flatten positive subsums
//  case SumExpr(plus, minus) if plus.exists(_.isInstanceOf[SumExpr]) =>
//    val subSums = plus.collect { case sum: SumExpr => sum}
//    SumExpr(plus.filterNot(e => subSums.contains(e)) ++ subSums.flatMap(_.plus), minus ++ subSums.flatMap(_.minus) )
//  // flatten negative subsums
//  case SumExpr(plus, minus) if minus.exists(_.isInstanceOf[SumExpr]) =>
//    val subSums = minus.collect { case sum: SumExpr => sum}
//    SumExpr(plus ++ subSums.flatMap(_.minus), minus.filterNot(e => subSums.contains(e)) ++ subSums.flatMap(_.plus) )
//}
//val sum_red = rule[Expr] {
//  // Substitution generation
//    case SumExpr(plus, minus) if plus.exists(_.isInstanceOf[SumExpr]) =>
//      val subSums = plus.collect { case sum: SumExpr => sum}
//      SumExpr(plus.filterNot(e => subSums.contains(e)) ++ subSums.flatMap(_.plus), minus ++ subSums.flatMap(_.minus) )
//    // flatten negative subsums
//    case SumExpr(plus, minus) if minus.exists(_.isInstanceOf[SumExpr]) =>
//      val subSums = minus.collect { case sum: SumExpr => sum}
//      SumExpr(plus ++ subSums.flatMap(_.minus), minus.filterNot(e => subSums.contains(e)) ++ subSums.flatMap(_.plus) )
//
//
//    case SumExpr(plus: List[LongValExpr], minus: List[LongValExpr]) if (plus++minus).nonEmpty && (plus++minus).forall(_.isInstanceOf[LongValExpr]) =>
//      LongValExpr(plus.map(_.v).sum - minus.map(_.v).sum)
//
//}
//def emitter = new OutputEmitter
//val normalizeFlat = somebu(log(flatten_red,"-flat-> ", emitter))
//val normalizeSum = somebu(log(sum_red,"-sum-> ", emitter))
//implicit def long2var(l: Long): LongValExpr = LongValExpr(l)
//implicit def string2var(s: String): VariableBindingExpr = VariableBindingExpr(s)
//
////normalizeFlat(SumExpr(SumExpr("x"::Nil, 2l::Nil)::Nil, 10l :: Nil))
//normalizeSum(SumExpr(SumExpr("x"::Nil, 2l::Nil)::Nil, 10l :: Nil))
////normalizeSum(SumExpr(SumExpr(15l::Nil, 2l::Nil)::Nil, 10l :: Nil))
//
