package com.kompreneble.scarete.condition

import com.kompreneble.scarete.condition.EvalService.EvalException
import com.typesafe.scalalogging.LazyLogging

case class EvalResult(unboundsOrResult: Either[Int, Boolean])

object EvalResult {
  def apply(unbounds: Int): EvalResult = EvalResult(Left(unbounds))
  def apply(result: Boolean): EvalResult = EvalResult(Right(result))

  def tru: EvalResult = EvalResult(true)
  def fals: EvalResult = EvalResult(false)
}

trait EvalService {

  /**
    * @param comparisons
    * @return either number of unbound variables or result
    */
  def eval(comparisons: List[Constraint]): EvalResult


}

object EvalService {
  case class EvalException(expr: Expr*) extends RuntimeException(s"Evaluation error on: ${expr.toList}")
  case class NotCanonicalFormEvalException(msg: String) extends RuntimeException(msg)
}

object NoEvalService extends EvalService with LazyLogging {
  /**
    * @param comparisons
    * @return either number of unbound variables or result
    */
  override def eval(comparisons: List[Constraint]): EvalResult = {
    def comparisonEval(e:Constraint): Boolean = e match {
      case Constraint(c, x: ValExpr, y: ValExpr) if x.tpe == y.tpe => c.compare(x,y)
      case Constraint(c, typ1: ValExpr, typ2: ValExpr) =>
        logger.error(s"Cant compare $e - different ValExpr types")
        throw EvalException(typ1, typ2)
      case _: Constraint =>
        throw EvalException(e.lhs, e.rhs)
    }

    EvalResult(comparisons.forall(comparisonEval))
  }

  val l = List[Int](1,2,3)

  l.map(_.toLong).sum
}