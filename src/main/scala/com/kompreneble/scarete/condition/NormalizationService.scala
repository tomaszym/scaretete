//package com.kompreneble.scarete.condition
//
//import org.kiama.rewriting.Rewriter._
//import org.kiama.rewriting.Strategy
//import org.kiama.util.OutputEmitter
//
//
//trait RichExpr {
//
//  implicit class RichSumElemList(sumElems: List[Expr]) {
//
//    def existsSum: Boolean = sumElems.exists(_.isInstanceOf[SumExpr])
//
//    def filterAndFoldSums: SumExpr = sumElems.filter(_.isInstanceOf[SumExpr]).map(_.asInstanceOf[SumExpr]).foldLeft(SumExpr()) {
//      case (xs, x) => xs + x
//    }
//
//    def filterNotSums: List[Expr] = sumElems.filterNot(_.isInstanceOf[SumExpr])
//
//  }
//
//}
//
//
//class NormalizationService   extends RichExpr {
//  val sum_reduction = rule[Expr] {
//
//    // flatten one positive elem sums
//    case SumExpr(head::Nil, Nil) => head
//
//
//    //flatten sum in sum
//    case SumExpr(plus, minus) if plus.existsSum => SumExpr(plus.filterNotSums, minus) + plus.filterAndFoldSums
//    case SumExpr(plus, minus) if minus.existsSum => SumExpr(plus, minus.filterNotSums) + minus.filterAndFoldSums
//
////    // flatten positive subsums
////    case SumExpr(plus, minus) if plus.exists(_.isInstanceOf[SumExpr]) =>
////      val subSums = plus.collect { case sum: SumExpr => sum}
////      SumExpr(plus.filterNot(e => subSums.contains(e)) ++ subSums.flatMap(_.plus), minus ++ subSums.flatMap(_.minus) )
////    // flatten negative subsums
////    case SumExpr(plus, minus) if minus.exists(_.isInstanceOf[SumExpr]) =>
////      val subSums = minus.collect { case sum: SumExpr => sum}
////      SumExpr(plus ++ subSums.flatMap(_.minus), minus.filterNot(e => subSums.contains(e)) ++ subSums.flatMap(_.plus) )
//
//    // Substitution generation
//    //    case SumExpr(plus: List[_], minus: List[_]) if (plus++minus).nonEmpty && (plus++minus).forall(_.isInstanceOf[ValExpr]) =>
//    //      (plus.asInstanceOf[List[ValExpr]], minus.asInstanceOf[List[ValExpr]]) match {
//    //        case strings if strings.forall(_.isString) =>
//    //      }
//
//
//
//  }
//
//  val e = new OutputEmitter
//
//  val normalizeStrategy: Strategy = log(sum_reduction,"-sum-> ", e)
//
//  def normalize(expr: Expr) = {
//    val normalized: Option[Expr] = normalizeStrategy(expr).map(_.asInstanceOf[Expr])
//
//    normalized.get
//  }
//}
