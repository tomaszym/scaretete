package com.kompreneble.scarete.condition

import com.kompreneble.scarete.condition.EvalService.EvalException
import com.kompreneble.scarete.condition.eval.normal.NormalRules
import com.kompreneble.scarete.fact.{Attr, AttrKey, AttrValue, Fact}
import shapeless.ops.nat.Prod

import scalaz.MonadPlus

sealed trait Expr {

  /** Binds the fact to LHS of an expression in alpha nodes.
    * In beta part should do nothing (because there aren't any `AttrExpr` anymore. */
  def bindOnAlpha(alphaFact: Fact):Expr

  def containsBindings: Boolean

  def substitute(v: VariableBindingExpr, expr: Expr): Expr
}

case class ValExprType(stringTpe: String)
case class ValExpr(tpe: ValExprType, v: Any) extends Expr {
  assert(!v.isInstanceOf[Int], "wtf, want a long here if you want an integer type!")

  override def bindOnAlpha(alphaFact: Fact): Expr = this

  override def substitute(v: VariableBindingExpr, expr: Expr): Expr = this

  override def containsBindings: Boolean = false

  override def toString: String = v.toString + tpe.stringTpe.head.toString

  def isString = tpe == ValExpr.StringType
  def isInt = tpe == ValExpr.IntegerType
  def isDecimal = tpe == ValExpr.DecimalType
}

object IntegerValExpr {
  def unapply(e: Expr): Option[Long] = e match {
    case ValExpr(ValExpr.IntegerType, i: Long) => Some(i)
    case els: Expr => None
  }
}
object DecimalValExpr {
  def unapply(e: Expr): Option[Double] = e match {
    case ValExpr(ValExpr.DecimalType, i: Double) => Some(i)
    case els: Expr => None
  }
}

object ValExpr {
  val StringType = ValExprType("string")
  def apply(s: String): ValExpr = ValExpr(StringType,s)
  val IntegerType = ValExprType("integer")
  def apply(i: Int): ValExpr = ValExpr(IntegerType, i.toLong)
  def apply(l: Long): ValExpr = ValExpr(IntegerType, l)
  val DecimalType = ValExprType("decimal")
  def apply(d: Double): ValExpr = ValExpr(DecimalType, d)
}

case class AttributeBindingException(attrExpr: AttrExpr, alphaFact: Fact) extends RuntimeException(s"Trying to bind $alphaFact to an attribute $attrExpr")

case class AttrExpr(stringAttr: String) extends Expr {
  override def containsBindings: Boolean = false
  override def bindOnAlpha(alphaFact: Fact): Expr = {
    def attributeValue: AttrValue = alphaFact.attributes.collect {
      case (attrName, AttrValue(value)) if attrName.stringName == stringAttr => AttrValue(value)
    }.head

    attributeValue match {
      case AttrValue(isString: String) => ValExpr(isString)
      case AttrValue(isDouble: Double)=> ValExpr(isDouble)
      case AttrValue(isLong: Long) => ValExpr(isLong)
      case AttrValue(isInt: Int) => ValExpr(isInt.toLong)
      case _: AttrValue => throw AttributeBindingException(this, alphaFact)
    }
  }

  override def substitute(v: VariableBindingExpr, expr: Expr): Expr = throw EvalException(this)
}

case class VariableBindingExpr(v: String) extends Expr {
  override def bindOnAlpha(alphaFact: Fact): Expr = this

  override def containsBindings: Boolean = true

  override def substitute(x: VariableBindingExpr, expr: Expr): Expr = if(x==this) expr else this

  override def toString: String = "$" + v
}
//

case class SumExpr(plus: List[Expr] = List(), minus: List[Expr] = List()) extends Expr {
  /** Binds the fact to LHS of an expression in alpha nodes.
    * In beta part should do nothing (because there aren't any `AttrExpr` anymore. */
  override def bindOnAlpha(alphaFact: Fact): Expr = SumExpr(
    plus = plus.map(_.bindOnAlpha(alphaFact)),
    minus = minus.map(_.bindOnAlpha(alphaFact))
  )

  def map(fun: Expr => Expr) = SumExpr(plus.map(fun), minus.map(fun))

  override def containsBindings: Boolean = plus.exists(_.containsBindings) || minus.exists(_.containsBindings)

  override def substitute(v: VariableBindingExpr, expr: Expr): Expr = SumExpr(
    plus = plus.map(_.substitute(v, expr)),
    minus = minus.map(_.substitute(v, expr))
  )

  override def toString: String = {
    val positive: String = plus.mkString("+")
    val negative: String = if(minus.nonEmpty){"-" + minus.mkString("-") } else ""
    "(" + positive + negative + ")"
  }
  def isEmpty = plus.isEmpty && minus.isEmpty

  def +(another: SumExpr): SumExpr = SumExpr(this.plus ++ another.plus, this.minus ++ another.minus)
  def -(another: SumExpr): SumExpr = SumExpr(this.plus ++ another.minus, this.minus ++ another.plus)
  def negate: SumExpr = SumExpr(minus, plus)

}

case class ProdExpr(multiply: List[Expr] = List(), divide: List[Expr] = List()) extends Expr with NormalRules {
  /** Binds the fact to LHS of an expression in alpha nodes.
    * In beta part should do nothing (because there aren't any `AttrExpr` anymore. */
  override def bindOnAlpha(alphaFact: Fact): Expr = ProdExpr(
    multiply = multiply.map(_.bindOnAlpha(alphaFact)),
    divide = divide.map(_.bindOnAlpha(alphaFact))
  )
  override def containsBindings: Boolean = multiply.exists(_.containsBindings) || divide.exists(_.containsBindings)

  override def substitute(v: VariableBindingExpr, expr: Expr): Expr = ProdExpr(
    multiply = multiply.map(_.substitute(v, expr)),
    divide = divide.map(_.substitute(v, expr))
  )

  override def toString: String = {
    val positive: String = multiply.mkString("*")
    val negative: String = if(divide.nonEmpty){"/" + divide.mkString("/") } else ""
    "(" + positive + negative + ")"
  }
  def isEmpty = multiply.isEmpty && divide.isEmpty

  def *(another: ProdExpr): ProdExpr = ProdExpr(this.multiply ++ another.multiply, this.divide ++ another.divide)
  def /(another: ProdExpr): ProdExpr = ProdExpr(this.multiply ++ another.divide, this.divide ++ another.multiply)

  def *(e: Expr): ProdExpr = e match {
    case p: ProdExpr => this / p.inverse
    case els: Expr => this.copy(multiply = e::multiply)
  }
  def /(e: Expr): ProdExpr = e match {
    case ProdExpr(thatMultiply, thatDivide) =>
      import org.kiama.rewriting.Rewriter._
      ProdExpr(multiply ++ thatDivide, divide ++ thatMultiply).applyStrategy(repeat(prodToProdStrategy)) match {
        case p: ProdExpr => p
        case els: Expr => throw TransformationException("Expecting only ProdExpr to come out after simplification")
      }
    case els:Expr => this.copy(divide = e :: this.divide)
  }


  def inverse: ProdExpr = ProdExpr(divide, multiply)

}


case class Relations(cs: List[Constraint]) extends Expr {
  /** Binds the fact to LHS of an expression in alpha nodes.
    * In beta part should do nothing (because there aren't any `AttrExpr` anymore. */
  override def bindOnAlpha(alphaFact: Fact): Expr = copy(cs.map(_.bindOnAlphaInComparison(alphaFact)))

  override def substitute(v: VariableBindingExpr, expr: Expr): Expr = ???

  override def containsBindings: Boolean = cs.exists(c => c.lhs.containsBindings || c.rhs.containsBindings)
}

case class Sub(wCzym: Relations, zaCo: VariableBindingExpr, co: Expr) extends Expr {
  /** Binds the fact to LHS of an expression in alpha nodes.
    * In beta part should do nothing (because there aren't any `AttrExpr` anymore. */
  override def bindOnAlpha(alphaFact: Fact): Expr = throw EvalException(this)

  override def substitute(v: VariableBindingExpr, expr: Expr): Expr = ???

  override def containsBindings: Boolean = ???
}

