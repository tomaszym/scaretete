package com.kompreneble.scarete.condition

case class TransformationException(msg: String) extends Exception(msg)
