package com.kompreneble.scarete.condition

case class ProductionId(productionNameString: String) extends AnyVal

case class ProductionRule(constraintTree: ConstraintTree, effect: ProductionId)