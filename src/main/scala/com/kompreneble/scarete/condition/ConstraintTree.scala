package com.kompreneble.scarete.condition

import java.util.NoSuchElementException

import com.kompreneble.scarete.fact.{Fact, FactType}

trait ConstraintTree {
  def constraintChains: List[ConstraintChain]
}

/** Beta nodes in RETE nomenclature
  *
  * @param connections
  */
case class ConstraintBranch(connections: List[ConstraintTree]) extends ConstraintTree {
  override def constraintChains: List[ConstraintChain] = connections.flatMap(_.constraintChains)
}

/** List of alpha nodes in RETE nomenclature
  *
  * @param tpe
  * @param constraints
  */
case class ConstraintChain(tpe: FactType, constraints: List[Constraint]) extends ConstraintTree {
  def head = constraints.head
  def tail = ConstraintChain(tpe, constraints.tail)




  override def constraintChains: List[ConstraintChain] = this :: Nil

  /** Drops all the constraints until finds a desired cut and then returns the rest without the cut
    *
    * @param cutOut element after which elements will be returned
    * @return
    */
  def cutAt(cutOut: Constraint) = {
    val constraintsLeft = constraints.dropWhile(_ != cutOut) match {
      case Nil => throw new NoSuchElementException(s"There's no constraint $cutOut in this chain: $constraints")
      case _ :: Nil => Nil
      case _:: tail => tail
    }
    this.copy(constraints = constraintsLeft)
  }
}

case class Constraint(comparator: Comparator, lhs: Expr, rhs: Expr) {

  /** Binds the fact to LHS of an expression in alpha nodes.
    * In beta part should do nothing (because there aren't any `AttrExpr` anymore. */
  def bindOnAlphaInComparison(alphaFact: Fact): Constraint = copy(lhs = lhs.bindOnAlpha(alphaFact), rhs = rhs.bindOnAlpha(alphaFact))

  def substituteInComparison(v:VariableBindingExpr, expr: Expr) = copy(lhs = lhs.substitute(v, expr), rhs = rhs.substitute(v, expr))

  val containsBinding: Boolean = lhs.containsBindings || rhs.containsBindings

  override def toString: String = s"$lhs $comparator $rhs"
}