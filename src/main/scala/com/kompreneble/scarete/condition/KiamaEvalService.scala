package com.kompreneble.scarete.condition

import com.kompreneble.scarete.condition.EvalService.{EvalException, NotCanonicalFormEvalException}
import com.kompreneble.scarete.condition.eval.canonical.{CanonicalFormPatterns, CanonicalFormSumReductionStrategy, CanonicalPolynomialRules}
import com.kompreneble.scarete.condition.eval.EvalUtil
import com.typesafe.scalalogging.StrictLogging
import kamon.Kamon
import kamon.trace.Tracer
import org.kiama.rewriting.Strategy
import org.kiama.util.{OutputEmitter, StringEmitter}

import scala.collection.immutable.::
import scalaz.Alpha.N

object KiamaEvalService extends EvalService with CanonicalPolynomialRules with StrictLogging with EvalUtil {

  import org.kiama.rewriting.Rewriter._

  object ExtractX {
    /** Extracts positive expression which contains variable binding
      * to be substituted.
      */
    def unapply(c: Constraint): Option[(VariableBindingExpr, Comparator, Expr)] = {

      c.lhs match {
          // trivial case:
        case v: VariableBindingExpr if !c.rhs.freeVariables.contains(v)=>
          //if there is the same variable on the other side
          if(freeVariables(c.rhs).contains(v)) None //throw NotCanonicalFormEvalException(s"Expression not in canonical polynominal form: $v both on lhs and rhs in: $c")
          else Some((v,c.comparator,c.rhs))
//        case p: ProdExpr if p.freeVariables.size == 1 => Some((p, c.comparator, c.rhs))

        case ProdExpr(mul, div) if (mul++div).flatMap(_.freeVariables).nonEmpty =>
          mul.collectFirstVariableBinding match {
              // There is a variable binding above fraction, rhs not flipped
            case Some(s) =>
              val rhs = ProdExpr(c.rhs :: div, mul.diff(List(s)))
              Some((s, c.comparator, rhs))

              // There is no variable binding above fraction, lets try below
            case None =>
              div.collectFirstVariableBinding match {
                case Some(s) =>
                  val rhs = ProdExpr(mul, c.rhs :: div.diff(List(s)))
                  Some((s, c.comparator, rhs))

                case None => None//throw NotCanonicalFormEvalException("Can't extract variable binding")
              }
          }

//        case s: SumExpr if s.freeVariables.size == 1 => Some((s, c.comparator, c.rhs))
        case SumExpr(plus,minus) if (plus ++ minus).flatMap(_.freeVariables).nonEmpty =>

          plus.collectFirstVariableBinding match {
            // There is a positive variable binding
            case Some(s) =>
              val rhs = SumExpr(c.rhs :: minus, plus.diff(List(s)))
              Some((s, c.comparator, rhs))

            // There is no variable binding above fraction, lets try below
            case None =>
              minus.collectFirstVariableBinding match {
                case Some(s) =>
                  val rhs = SumExpr(plus, c.rhs::minus.diff(List(s)))
                  Some((s, c.comparator, rhs))

                case None => None//throw NotCanonicalFormEvalException("Can't extract variable binding")
              }
          }
        case els: Expr => None
      }
    }
  }
//  object ExtractMinusX {
//    /** Extracts negative expression which contains variable binding
//      * to be substituted.
//      */
//    def unapply(s: SumExpr): Option[(Expr, SumExpr)] = s.minus.collectFirst{ case x: VariableBindingExpr => x}.map { x =>
//      (x, s.copy(minus = s.minus.diff(List(x))))
//    }
//  }


  val substitueStrategy = log(rule[Expr] {
    // Substitution generation
    case Relations(Constraint(Eq, lhs, v: VariableBindingExpr) :: tail) =>
      Sub(Relations(tail), v, lhs)//Relations(tail.map(rel => Sub(rel,v, rhs)))
    case Relations(Constraint(Eq, v: VariableBindingExpr, rhs) :: tail) =>
      Sub(Relations(tail), v, rhs)//Relations(tail.map(rel => Sub(rel,v, rhs)))

    case r@Relations(Constraint(Eq, lhs, rhs) :: tail) if r.isNumericExpr && rhs.freeVariables.nonEmpty =>
      Relations(Constraint(Eq, SumExpr(lhs::Nil, rhs::Nil), ValExpr(0)) :: tail)

    // extract x
    case Relations(ExtractX(lhs, Eq, rhs) :: otherComparisons) => Sub(Relations(otherComparisons),lhs,rhs)

    // iterate over relations
    case Relations(head :: tail) if freeVariables(Relations(List(head))).isEmpty && freeVariables(Relations(tail)).nonEmpty => Relations(tail :+ head)

    case Sub(m, x, n) if !(m.freeVariables contains (x)) => m  // Garbage collection
    case Sub(relations, v, expr) => Relations(relations.cs.map(_.substituteInComparison(v, expr)))    // Explicit substitution

  }, "sub--> ", em)



  def simplify(relations: Relations, limit: Int = 5): EvalResult = {
//    assert(relations.cs.nonEmpty)
//    logger.debug(s"Simplifying $relations")
    val normalized: Relations = repeat(normalizeStrategy)(relations).map(_.asInstanceOf[Relations]).get
    val canonical: Relations = repeat(canonicalizationStrategy)(normalized).map(_.asInstanceOf[Relations]).get
    val normalizedCanonical: Relations = repeat(normalizeStrategy)(canonical).map(_.asInstanceOf[Relations]).get
    val res: Relations = repeat(substitueStrategy)(normalizedCanonical).map(_.asInstanceOf[Relations]).get


//    logger.debug(s"${res}")
//    val res: Option[Relations] = repeat(sumReductionStrategy)(repeat(normalizeStrategy)(repeat(substitueStrategy)(relations))).flatMap(_.asInstanceOf[Some[Some[Relations]]]).flatten

    if(res == relations) NoEvalService.eval(relations.cs)
    else simplify(res, limit-1)
  }
//  def simplifyInnermost(relations: Relations) = {
//    val normalized: Option[Relations] = reduce(sum_reduction)(relations).map(_.asInstanceOf[Relations])
//
//
//    NoEvalService.eval(normalized.get.cs)
//  }


  val myHistogram = Kamon.metrics.histogram("eval")
  /**
    * @param comparisons
    * @return either number of unbound variables or result
    */
  override def eval(comparisons: List[Constraint]): EvalResult = {
    val begin = System.nanoTime()

    val result = simplify(Relations(comparisons))
    val end = System.nanoTime()
    myHistogram.record(end-begin)
    result
  }
//  def evalInnermost(comparisons: List[Comparison]): EvalResult = simplifyInnermost(Relations(comparisons))

}