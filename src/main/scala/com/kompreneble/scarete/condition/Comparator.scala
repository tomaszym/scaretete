package com.kompreneble.scarete.condition

import ValExpr._
import com.kompreneble.scarete.fact.Fact

sealed abstract class Comparator {
  def compare(l: ValExpr, r: ValExpr): Boolean
}

case object Eq extends Comparator {
  override def compare(l: ValExpr, r: ValExpr): Boolean = l == r
  
  override def toString: String = "="
}
case object NEq extends Comparator {
  override def compare(l: ValExpr, r: ValExpr): Boolean = l != r
}

case object Lt extends Comparator {
  override def compare(l: ValExpr, r: ValExpr): Boolean = {
    (l.tpe, r.tpe) match {
      case (StringType, StringType) => l.v.asInstanceOf[String] < r.v.asInstanceOf[String]
      case (IntegerType, IntegerType) => l.v.asInstanceOf[Long] < r.v.asInstanceOf[Long]
      case (DecimalType, DecimalType) => l.v.asInstanceOf[Double] < r.v.asInstanceOf[Double]
    }
  }
}
case object LEt extends Comparator {
  override def compare(l: ValExpr, r: ValExpr): Boolean = ???
}
case object Gt extends Comparator {
  override def compare(l: ValExpr, r: ValExpr): Boolean = ???
}
case object GEt extends Comparator {

  override def compare(l: ValExpr, r: ValExpr): Boolean = ???
}