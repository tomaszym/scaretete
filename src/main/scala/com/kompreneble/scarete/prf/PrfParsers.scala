package com.kompreneble.scarete.prf

import com.kompreneble.scarete.prf.ast._

import scala.util.parsing.combinator.PackratParsers
import scala.util.parsing.combinator.lexical.StdLexical
import scala.util.parsing.combinator.syntactical.StandardTokenParsers

class PrfParsers extends StandardTokenParsers with PackratParsers {

  override val lexical: StdLexical = new PRFLexicalParsers

  lazy val parseRoot: PackratParser[PrfRoot] = rep(parseRule) ^^ {
    case rules => PrfRoot(rules)
  }
  lazy val parseRule: PackratParser[PrfRule] = parseRuleName ~ rep(parseEntityConditions) ~ parseEffect ^^ {
    case ruleName ~ conditions ~ effect =>
      PrfRule(
        ruleName,
        conditions,
        effect)
  }

  lazy val parseRuleName: PackratParser[String] = "rule" ~> stringLit <~ "if" ~ ":"

  lazy val parseEntityConditions: PackratParser[PrfEntityConditions] = (("in" ~> ident) <~ "(") ~ (repsep(parseCondition, ",") <~ ")") ^^ {
    case tpe ~ conditions => PrfEntityConditions(tpe, conditions)
  }

  /**
    * Condition
    *
    */
    lazy val parseCondition: PackratParser[PrfArithmeticCondition] = parseExpr ~ "=" ~ parseExpr ^^ {
      case lhs ~ op ~ rhs => PrfArithmeticCondition(op, lhs, rhs)
  }

  def parseParens[T](p: PackratParser[T]): PackratParser[T] = ("(" ~> p <~ ")") named "parseParens"

  lazy val parseExpr: PackratParser[PrfExpr] = parseParens(parseExpr) | parseSum | parseProd | parseValue
  lazy val parseExprNoProd: PackratParser[PrfExpr] = parseParens(parseExpr) | parseSum | parseValue
  lazy val parseExprNoSum: PackratParser[PrfExpr] = parseParens(parseExpr) | parseProd | parseValue
  lazy val parseExprNoSumNoProd: PackratParser[PrfExpr] = parseParens(parseExpr) | parseValue

  /**
    * Sum
    *
    */
  lazy val parseSumOpString: PackratParser[String] = ("+" | "-")
  lazy val parseSumOp: PackratParser[PrfSumOp] = parseSumOpString ^^ {
    case "+" => PrfPlus
    case "-" => PrfMinus
  }
  lazy val parseSumElem: PackratParser[PrfSumElem] = parseSumOp ~ parseExprNoSum ^^ {
    case op ~ prod => PrfSumElem(op, prod)
  }

  lazy val parseSum: PackratParser[PrfSum] = opt(parseSumOp) ~ parseExprNoSum ~ rep1(parseSumElem) ^^ {
    case maybeOp ~ prod ~ tail => {
      val head = maybeOp match {
        case None => PrfSumElem(PrfPlus, prod)
        case Some(op) => PrfSumElem(op, prod)
      }

      PrfSum(head :: tail)
    }
  } named "parseSum"

  /**
    * Prod
    *
    */
  lazy val parserProdOpString: PackratParser[String] = ("*" | "/") named "parseProdOpString"
  lazy val parseProdOp: PackratParser[PrfProdOp] = parserProdOpString ^^ {
    case "*" => PrfMul
    case "/" => PrfDiv
  } named "parseProdOp"
  lazy val parseProdElem: PackratParser[PrfProdElem] = parseProdOp ~ parseExprNoSumNoProd ^^ {
    case op ~ elem =>PrfProdElem(op, elem)
  } named "parseProdElem"

  lazy val parseProd: PackratParser[PrfProd] = parseExprNoSumNoProd ~ rep1(parseProdElem) ^^ {
    case head ~ tail => PrfProd(PrfProdElem(PrfMul, head) :: tail)
  }

  /**
    * Values
    *
    */

  lazy val parseStringLiteral: PackratParser[PrfStringLiteral] = stringLit ^^ { case s => PrfStringLiteral(s) } named "parseStringLiteral"
  lazy val parseIntLiteral: PackratParser[PrfIntLiteral] = numericLit ^^ { case s => PrfIntLiteral(s.toInt) } named "parseIntLiteral"
  lazy val parseDecimalLiteral: PackratParser[PrfDecimalLiteral] = numericLit ~ "." ~ numericLit ^^ {
    case x ~ y ~ z => PrfDecimalLiteral(s"$x$y$z".toDouble)
  } named "parseDecimalLiteral"
  lazy val parseBinding: PackratParser[PrfBinding] =  keyword("$") ~> ident ^^ { case s => PrfBinding(s) } named "parseBinding"
  lazy val parseAttribute: PackratParser[PrfAttribute] = ident ^^ {case attr => PrfAttribute(attr)} named "parseAttribute"
  lazy val parseFun: PackratParser[PrfFun] = ident ~ ("(" ~> repsep(parseExpr, ",") <~  ")") ^^ {
    case name ~ args => PrfFun(name, args:_*)
  } named "parseFun"

  lazy val parseValue: PackratParser[PrfValue] = (parseDecimalLiteral | parseIntLiteral | parseStringLiteral | parseBinding | parseFun | parseAttribute) named "parseValue"

  /**
    * Effect
    *
    */
  lazy val parseEffect: PackratParser[PrfEffect] = ("then" ~> ("(" ~> ident <~ ")" )) ^^ {
    case effect => PrfEffect(effect)
  }

  def parseRootString(input: String): Option[PrfRoot] = parseString(parseRoot, input)
  def parseSumString(input: String): Option[PrfSum] = parseString(parseSum, input)
  def parseProdString(input: String): Option[PrfProd] = parseString(parseProd, input)
  def parseValString(input: String): Option[PrfValue] = parseString(parseValue, input)


  def parseString[T](parser: Parser[T], input : String) : Option[T] = {

    phrase(parser)(new lexical.Scanner(input)) match {
      case Success(result, _) =>
        Some(result)
      case Failure(msg, next) => println(s"In $parser: $msg at position ${next.pos}."); None // TODO log
      case _ => None
    }
  }

  /**
    *  A parser generator delimiting whole phrases (i.e. programs).
    *
    *  Overridden to make sure any input passed to the argument parser
    *  is wrapped in a `PackratReader`.
    */
  override def phrase[T](p: Parser[T]) = {
    val q = super.phrase(p)
    new PackratParser[T] {
      def apply(in: Input) = in match {
        case in: PackratReader[_] => q(in)
        case in => q(new PackratReader(in))
      }
    }
  }


}
