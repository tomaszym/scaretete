package com.kompreneble.scarete.prf

import com.kompreneble.scarete.prf.ast.{PrfMul, PrfProdElem, _}

import scala.util.Properties

trait PrfUtils {

  private val prfDirectory = "rules"
  private val ext = "prf"

  /** Loads a file .prf from resources
 *
    * @param name with no extension
    */
  def loadPrf(name: String): String = {
    val path = s"/$prfDirectory/$name.$ext"
    val url = this.getClass.getResource(path)
    val source = io.Source.fromURL(url, "UTF-8")

    source.getLines().mkString(Properties.lineSeparator)
  }


  private val prfParsers = new PrfParsers
  def parsePrfRootFromResources(filename: String): Option[PrfRoot] = prfParsers.parseRootString(loadPrf(filename))
  def parseRule(stringRule: String): PrfRule = prfParsers.parseRootString(stringRule).get.rules.head
  def parseSum(stringPrf: String): PrfSum = prfParsers.parseSumString(stringPrf).get
  def parseProd(stringPrf: String): PrfProd = prfParsers.parseProdString(stringPrf).get
  def parseVal(stringPrf: String): PrfValue = prfParsers.parseValString(stringPrf).get



  implicit class SearchRuleInRootPrf(root: PrfRoot) {
    def getRule(ruleName: String): PrfRule = root.rules.find(_.name == ruleName).get
  }


  implicit def intLiteral(i: Int): PrfIntLiteral = PrfIntLiteral(i)

  implicit def attrToSum(a: PrfAttribute) = wrapSum(a)
  implicit def bindingToSum(a: PrfBinding) = wrapSum(a)


  def wrapSum(prod: PrfProd): PrfSum = PrfSum(List(PrfSumElem(PrfPlus, prod)))
  def wrapSum(v: PrfValue): PrfSum = PrfSum(List(PrfSumElem(PrfPlus, wrapProd(v))))
  def wrapProd(v:PrfValue): PrfProd = PrfProd(List(PrfProdElem(PrfMul, v)))


}
