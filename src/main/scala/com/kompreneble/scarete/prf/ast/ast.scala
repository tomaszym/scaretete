package com.kompreneble.scarete.prf.ast

case class PrfRoot(rules: List[PrfRule]) {
  def getRule(name: String): PrfRule =
    rules.find(_.name == name)
         .getOrElse(throw new NoSuchElementException(s"Theres no rule $name"))
}

case class PrfRule(name: String, conditions: List[PrfEntityConditions], effects: PrfEffect)

case class PrfEntityConditions(tpe: String, conditions: List[PrfArithmeticCondition])

case class PrfArithmeticCondition(op: String, lhs: PrfExpr, rhs: PrfExpr)

trait PrfExpr

sealed abstract class PrfSumOp; case object PrfPlus extends PrfSumOp; case object PrfMinus extends PrfSumOp
case class PrfSumElem(op: PrfSumOp, elem: PrfExpr)
case class PrfSum(elems: List[PrfSumElem]) extends PrfExpr

sealed abstract class PrfProdOp; case object PrfMul extends PrfProdOp; case object PrfDiv extends PrfProdOp
case class PrfProdElem(op: PrfProdOp, elem: PrfExpr)
case class PrfProd(elems: List[PrfProdElem]) extends PrfExpr

sealed trait PrfValue extends PrfExpr
case class PrfAttribute(attr: String) extends PrfValue
case class PrfStringLiteral(s: String) extends PrfValue
case class PrfIntLiteral(i: Int) extends PrfValue
case class PrfDecimalLiteral(d: Double) extends PrfValue
case class PrfBinding(s: String) extends PrfValue
case class PrfFun(name: String, args: PrfExpr*) extends PrfValue

case class PrfEffect(dummy: String)
