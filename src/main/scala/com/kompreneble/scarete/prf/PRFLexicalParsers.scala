package com.kompreneble.scarete.prf

import scala.collection.mutable
import scala.util.parsing.combinator.lexical.StdLexical

class PRFLexicalParsers extends StdLexical {
  override val reserved: mutable.HashSet[String] = mutable.HashSet("rule", "if", "in", "then", "=", "+", "-", "*", "/")

  override val delimiters = mutable.HashSet(":().,$".toList.map(_.toString):_*)

  override def identChar: Parser[Char] = super.identChar | elem("equals hack until operator scanning support", e => "=+-*/".contains(e))

}
