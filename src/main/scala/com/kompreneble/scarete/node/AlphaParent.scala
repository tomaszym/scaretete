package com.kompreneble.scarete.node

import akka.actor.{Actor, ActorRef, Props}
import com.kompreneble.scarete.condition.{Constraint, ConstraintChain, EvalService}
import com.kompreneble.scarete.fact.FactType

import scala.collection.mutable
import scala.util.Random

/** Encapsulates handling alpha sub actors
  *
  * Keeps both maps
  *  type -> actoRef and
  *  constraint -> actorRef
  *
  *  as different nodes have different query needs.  *
  */
trait AlphaParent { this: Actor =>

  val reteActor: ActorRef

  case class AlphaParentKey(tpe: FactType, constraint: Constraint)

  private val constraintMap: mutable.Map[AlphaParentKey, ActorRef] = mutable.HashMap()
  private val typeMap: mutable.Map[FactType, List[ActorRef]] = mutable.HashMap()
  private def updateTypeMap(factType: FactType, actorRef: ActorRef): Unit = {
    val before = typeMap.getOrElse(factType, Nil)
    typeMap.update(factType, actorRef :: before)
  }

  val evalService: EvalService

  private def spawnAlphaChild(tpe: FactType, constraint: Constraint): ActorRef = {

    val props: Props = AlphaActor.props(tpe, constraint, reteActor, evalService)

    val baby = context.actorOf(props,s"alpha-${tpe.stringType}-${constraintMap.size}")
    updateTypeMap(tpe, baby)
    baby
  }


//  @deprecated("ambigous :/")
//  def getChildRef(chain: ConstraintChain): ActorRef = constraintMap.getOrElseUpdate(chain.head, spawnAlphaChild(chain.tpe, chain.head))

  def getChildRef(tpe: FactType, c: Constraint): ActorRef ={
    constraintMap.getOrElseUpdate(AlphaParentKey(tpe, c), spawnAlphaChild(tpe, c))
  }

  def findChildRef(tpe: FactType): List[ActorRef] = typeMap.getOrElse(tpe, Nil)

  def allChildren: List[ActorRef] = constraintMap.values.toList

}
