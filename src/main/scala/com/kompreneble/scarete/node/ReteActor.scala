package com.kompreneble.scarete.node

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Stash}
import com.kompreneble.scarete.condition.{Constraint, ConstraintTree, EvalService, ProductionRule}
import com.kompreneble.scarete.encoder.FactEncoder
import com.kompreneble.scarete.fact.Fact
import com.kompreneble.scarete.node.BetaActor.ConnectAlpha
import com.kompreneble.scarete.node.QueryHandlerActor.{QueryInit, QueryPartialResult}
import com.kompreneble.scarete.node.ReteActor.{ProductionConnected, Propagate, ReteInsert}

class ReteActor(val productions: List[ProductionRule], val evalService: EvalService) extends Actor with Stash with AlphaParent with ActorLogging {

  val reteActor = self

  val productionActors: List[ActorRef] = productions.zipWithIndex.map{ case (production,idx) =>
    context.actorOf(ProductionActor.props(production, context.self, evalService), s"production-actor-$idx")
  }

  val queryHandlerActor: ActorRef = context.actorOf(QueryHandlerActor.props, s"query-handler-actor")

  override def receive: Receive = disconnected(productions.map(_.constraintTree))

  def connected: Receive = {

    case ReteInsert(fact) if fact.isQuery =>
//      log.debug(s"Rete query: ${findChildRef(fact.tpe)}")
//      log.debug(QueryInit(fact, productions, sender()).toString)
      queryHandlerActor ! QueryInit(fact, productions, sender())
      propagate(fact)

    case ReteInsert(fact) =>
      propagate(fact)

    case p: QueryPartialResult =>
      queryHandlerActor ! p
  }

  def propagate(fact: Fact) = {
    findChildRef(fact.tpe).foreach { actorRef =>
//      log.debug(s"Sending ${fact.tpe.stringType} from ReteActor to AlphaActor $actorRef")
      actorRef ! Propagate.initialize(fact)
    }
  }

  def disconnected(connectionsMissing: List[ConstraintTree]): Receive = {

    case msg@ConnectAlpha(chain, productionId, connectWith) =>
      val requestDestination = getChildRef(chain.tpe, chain.head)
//      log.debug(s"Passing connection request from $connectWith to $requestDestination")
      requestDestination ! msg

    case ProductionConnected(newConnection) =>
      connectionsMissing.filterNot(_ == newConnection) match {
        case Nil =>
//          log.debug("Rete network fully connected. Hurray!")

          unstashAll()
          context.become(connected)
        case moreToBeConnected => context.become(disconnected(moreToBeConnected))
      }

    case msg: Any => stash()
  }
}

object ReteActor {


  def props(productions: List[ProductionRule], evalService: EvalService): Props = Props(new ReteActor(productions, evalService))

  case class Propagate(states: List[State], query: Option[Fact] = None) {
    def stateUpdated(newConstraint: Constraint): Propagate = copy(states = states.map{ state =>
      state.copy(stateContext = StateContext(state.stateContext.alphaChain.copy(constraints = state.stateContext.alphaChain.constraints.:+(newConstraint))))
    })
  }

  object Propagate {
    def initialize(fact: Fact) = {
      val query = if(fact.isQuery) Some(fact) else None
      Propagate(State.empty(fact)::Nil, query = query)
    }
  }
  case class ReteInsert(queryFact: Fact)
  case class ProductionConnected(production: ConstraintTree)


}