package com.kompreneble.scarete.node

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.kompreneble.scarete.condition.{ConstraintTree, ProductionId, ProductionRule}
import com.kompreneble.scarete.fact.Fact
import com.kompreneble.scarete.node.QueryHandlerActor.{QueryInit, QueryPartialResult}

/** Handles queries:
  *  - stores queries in progress
  *  - on successful execution notifies the sender
  */
class QueryHandlerActor extends Actor with ActorLogging {

  case class QueryInProgress(
                              sender: ActorRef,
                              productions: List[ProductionId],
                              results: List[ProductionResult]) {
    def record(newResult: ProductionResult) = copy(results = newResult :: results)

    def toQueryResult: QueryResult = {
      val (on, off) = results.partition(_.result)
      QueryResult(on.map(_.productionId).toSet, off.map(_.productionId).toSet)
    }

    def isDone = productions.size == results.size
  }

  private[this] var queriesInProgress: Map[Fact, QueryInProgress] = Map()

  override def receive: Receive = {
    case QueryInit(fact, productions, sender) =>
//      log.debug(s"QueryInit $fact")
      queriesInProgress = queriesInProgress.+((fact, QueryInProgress(sender, productions.map(_.effect), List())))

    case QueryPartialResult(fact, result) =>
//      log.debug(s"PartialResult $fact@$result")
      val previousState: QueryInProgress = queriesInProgress(fact)
      val newState = previousState.record(result)
      if(newState.isDone) {
//        log.debug(s"Done: $fact")
        newState.sender ! newState.toQueryResult
        queriesInProgress = queriesInProgress.filterKeys(_ != fact)
      } else {
        queriesInProgress = queriesInProgress.updated(fact, newState)
      }
  }
}

object QueryHandlerActor {
    def props: Props = {
      Props(new QueryHandlerActor)
    }

    case class QueryInit(fact: Fact, productions: List[ProductionRule], sender: ActorRef)
  case class QueryPartialResult(fact: Fact, result: ProductionResult)
}