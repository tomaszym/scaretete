package com.kompreneble.scarete.node

import com.kompreneble.scarete.condition.ProductionId

case class QueryResult(on: Set[ProductionId], off: Set[ProductionId])
case class ProductionResult(productionId: ProductionId, result: Boolean)
object ProductionResult {
  def negative(productionId: ProductionId) = ProductionResult(productionId, result = false)
  def positive(productionId: ProductionId) = ProductionResult(productionId, result = true)
}
