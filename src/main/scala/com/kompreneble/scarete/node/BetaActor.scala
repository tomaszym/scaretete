package com.kompreneble.scarete.node

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Stash}
import com.kompreneble.scarete.condition._
import com.kompreneble.scarete.node.BetaActor.{AlphaConnected, BetaConnected, ConnectAlpha}
import com.kompreneble.scarete.node.QueryHandlerActor.QueryPartialResult
import com.kompreneble.scarete.node.ReteActor.Propagate

import scalaz._
import Scalaz._

class BetaActor(val next: ActorRef, thisConnections: List[ConstraintTree], productionId: ProductionId, reteActor: ActorRef, evalService: EvalService) extends Actor with Stash with ActorLogging {


  val alphaInputs: List[ConstraintChain] = thisConnections.collect {
    case a: ConstraintChain => a
  }
//  log.debug(s"AlphaInputs: $alphaInputs")

  val betaInputs: List[ConstraintBranch] = thisConnections.collect {
    case b: ConstraintBranch => b
  }

  val situationContexts: List[SituationContext] = thisConnections.map(connection => SituationContext(connection.constraintChains.map(StateContext).toSet))
  val betaMemory = new BetaMemory(situationContexts)

  thisConnections.foreach {
    case a: ConstraintChain =>
//      log.debug(s"Sending request to connect with alpha (${a.tpe}).")
      reteActor ! ConnectAlpha(a, productionId, context.self)
    case b@ConstraintBranch(connections) =>
//      log.debug("Creating subbeta node.")
      val c = context.actorOf(BetaActor.props(context.self, connections, productionId, reteActor, evalService), "b")



  }

  def fullyConnected: Receive = {
    case p@Propagate(states, queryFactMaybe) =>
      val newSituation = Situation(states.toSet)
      betaMemory.memorize(newSituation)

      val msOpt: Option[Map[SituationContext, List[Situation]]] = betaMemory.query(newSituation)

      if(msOpt.isEmpty) {
        queryFactMaybe match {
          case Some(queryFact) => reteActor ! QueryPartialResult(queryFact, ProductionResult.negative(productionId))
          case None => /// nothing
        }
      }
      for {
        ms <- msOpt
        situations <- cartesianProduct(ms.values.toList)
      } {

        val actualSituation = situations.map(_.filteredWithoutBindings).reduce(_ + _)


        val constraints = actualSituation.states.toList.flatMap(_.bindFacts)

//        log.debug(s"Constraints to be evaled: ${constraints}")
        evalService.eval(constraints) match {
          case EvalResult(Left(i)) =>
            val newStates = situations.reduce(_ + _).states.toList
            next ! Propagate(states = newStates, query = newStates.map(_.fact).find(_.isQuery)  )
          case EvalResult(Right(true)) =>
//            log.debug(s"Eval positive: ${states.map(_.fact)}")
            val newStates = situations.reduce(_ + _).states.toList
            next ! Propagate(states = newStates, query = newStates.map(_.fact).find(_.isQuery)  )
          case EvalResult(Right(false)) =>
            queryFactMaybe match {
              case Some(queryFact) => reteActor ! QueryPartialResult(queryFact, ProductionResult.negative(productionId))
              case None => // Nothing
            }
//            log.debug(s"Eval negative")
        }

      }

  }

  def cartesianProduct(ll: List[List[Situation]]) = ll.foldLeft(List(List.empty[Situation]))((l, r) => (l |@| r)(_ :+ _))
  def partiallyConnected(
                          connectedAlphas: List[ConstraintChain],
                          disconnectedAlphas: List[ConstraintChain],
                          connectedBetas: List[ConstraintBranch],
                          disconnectedBetas: List[ConstraintBranch]
                        ): Receive = {

    case AlphaConnected(newConnection) =>

//      log.debug(s"Alpha-connected $newConnection")
      disconnectedAlphas.filterNot( _ == newConnection) match {
        case Nil if disconnectedBetas.isEmpty =>

          unstashAll()
          reportConnectedToParent()
          context.become(fullyConnected)
        case newDisconnected  if disconnectedAlphas.contains(newConnection)=>
          context.become{
            partiallyConnected(
              connectedAlphas = newConnection :: connectedAlphas,
              disconnectedAlphas = newDisconnected,
              connectedBetas = connectedBetas,
              disconnectedBetas = disconnectedBetas
            )
          }
//        case noChanges =>
      }

    case BetaConnected(newConnection) =>

//      log.debug(s"Beta-connected $newConnection")
      disconnectedBetas.filterNot( _ == newConnection) match {
        case Nil if disconnectedAlphas.isEmpty =>

          unstashAll()
          reportConnectedToParent()
          context.become(fullyConnected)
        case newDisconnected  if disconnectedBetas.contains(newConnection)=>
          context.become{
            partiallyConnected(
              connectedAlphas = connectedAlphas,
              disconnectedAlphas = disconnectedAlphas,
              connectedBetas = newConnection :: connectedBetas,
              disconnectedBetas = newDisconnected
            )
          }
//        case noChanges =>
      }
    case p: Propagate => stash()
  }

  def reportConnectedToParent() = next ! BetaConnected(ConstraintBranch(thisConnections))

  override def receive: Receive = partiallyConnected(Nil, alphaInputs, Nil, betaInputs)
}


object BetaActor {

  def props(next: ActorRef, connections: List[ConstraintTree], productionName: ProductionId, reteActor: ActorRef, evalService: EvalService) = {
    Props(new BetaActor(next, connections, productionName, reteActor, evalService))

  }

//  trait Connect
  case class ConnectAlpha(alpha: ConstraintChain, productionName: ProductionId, connectWith: ActorRef) {
    def withTailConstraints: ConnectAlpha = this.copy(alpha = alpha.tail)
  }

  case class AlphaConnected(chain: ConstraintChain)
  case class BetaConnected(branch: ConstraintBranch)
}