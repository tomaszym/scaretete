package com.kompreneble.scarete.node

import akka.actor.Actor.Receive
import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.kompreneble.scarete.condition._
import com.kompreneble.scarete.node.BetaActor.{AlphaConnected, BetaConnected, ConnectAlpha}
import com.kompreneble.scarete.node.QueryHandlerActor.QueryPartialResult
import com.kompreneble.scarete.node.ReteActor.{ProductionConnected, Propagate}

import scala.collection.mutable

class ProductionActor(production: ProductionRule, val reteActor: ActorRef, evalService: EvalService) extends Actor with ActorLogging {


  val maybeBetaChild: Option[ActorRef] = production.constraintTree match {
    case alpha: ConstraintChain =>
      reteActor ! ConnectAlpha(alpha, production.effect, context.self)
//      log.debug("ProductionActor connecting self to Alpha")
      None
    case ConstraintBranch(connections) =>
//      log.debug("ProductionActor creating Betas connecting self to it")
      val child = context.actorOf(BetaActor.props(context.self, connections, production.effect, reteActor, evalService), "beta")
      Some(child)
    }

  override def receive: Receive = disconnected

  def disconnected: Receive = {
    case AlphaConnected(_) =>

      reteActor ! ProductionConnected(production.constraintTree)
      context.become(connected)

    case BetaConnected(_) =>

      reteActor ! ProductionConnected(production.constraintTree)
      context.become(connected)


    case msg: Any =>

  }

  def connected: Receive = {
    case Propagate(states, None) =>
//      log.debug(s"Received nonquery propagation at production actor $states")

    case p@Propagate(states, Some(queryFact)) =>
      reteActor ! QueryPartialResult(queryFact, ProductionResult.positive(production.effect))
//      log.debug(s"Received at production actor $p")


  }
}

object ProductionActor {

  def props(production: ProductionRule, reteActorRef: ActorRef, evalService: EvalService): Props = {
    Props(new ProductionActor(production, reteActorRef, evalService))
  }

}