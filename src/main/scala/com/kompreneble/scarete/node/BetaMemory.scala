package com.kompreneble.scarete.node

import com.kompreneble.scarete.condition.ConstraintChain
import com.kompreneble.scarete.fact.Fact
import com.typesafe.scalalogging.LazyLogging

import scala.collection.mutable

case class StateContext(alphaChain: ConstraintChain) {
  def containsBinding: Boolean = alphaChain.constraints.exists(_.containsBinding)
}
case class State(fact: Fact, stateContext: StateContext) {

  def containsBinding = stateContext.containsBinding

  def bindFacts =
    if(fact.tpe == stateContext.alphaChain.tpe)
      stateContext.alphaChain.constraints.map(_.bindOnAlphaInComparison(fact))
  else
      throw new RuntimeException(s"Fact type doesnt match in ${this.toString}")
}
object State {
  def empty(fact: Fact) = State(fact, StateContext(ConstraintChain(fact.tpe, Nil)))
}

case class Situation(states: Set[State]) {
  def context: SituationContext = SituationContext(states.map(_.stateContext))

  def +(other: Situation) = Situation(states union other.states)

  lazy val filteredWithoutBindings: Situation = Situation(states.filter(_.containsBinding))
}
case class SituationContext(alphaChains: Set[StateContext])

class BetaMemory(val memorySlots: List[SituationContext]) extends LazyLogging {

  val memoryMap: Map[SituationContext, mutable.Queue[Situation]] = memorySlots.zip(List.fill(memorySlots.size)(mutable.Queue[Situation]())).toMap

  def memorize(s: Situation): Unit = memoryMap(s.context).enqueue(s)

  def forget(s: Situation): Unit = memoryMap(s.context).dequeueFirst(_ == s)


  /** Do we have at least one `Situation`
    * for every `SituationContext`?
 *
    * @return whether at least one valid propagation is in the memory for every connection the node has
    */
  def isQueryable: Boolean = {
    val result = memoryMap.forall {case (_, situations) => situations.nonEmpty}
//    logger.debug(s"Beta memory queryable: $result, ${memoryMap}")
    result
  }

  /** If there are facts in every context the memory
    * is interested in returns list of current memories.
    *
    * The given `Situation` is the only one available
    * for this `SituationContext` context.
    *
    * @return situations map, where for current context only one (the new one) is available
    */
  def query(newSituation: Situation): Option[Map[SituationContext, List[Situation]]] =
    if(isQueryable) Some { // WE'VE GOT A SITUATION!!!!
      memoryMap.mapValues(_.toList).updated(newSituation.context, newSituation::Nil)
    } else None


}
