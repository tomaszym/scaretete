package com.kompreneble.scarete.node

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.kompreneble.scarete.condition.{Constraint, EvalResult, EvalService, ProductionId}
import com.kompreneble.scarete.fact.FactType
import com.kompreneble.scarete.node.AlphaActor.AlphaPropagationException
import com.kompreneble.scarete.node.BetaActor.{AlphaConnected, ConnectAlpha}
import com.kompreneble.scarete.node.QueryHandlerActor.QueryPartialResult
import com.kompreneble.scarete.node.ReteActor.Propagate

import scala.collection.mutable

class AlphaActor(val alphaType: FactType, val thisConstraint: Constraint, val reteActor: ActorRef, val evalService: EvalService) extends Actor with ActorLogging with AlphaParent {
  val betaConnections = mutable.Queue[ActorRef]()

  var productions: Set[ProductionId] = Set()

  def receive: Receive = {

    case msg@ConnectAlpha(fullChain, productionId, connectMe) =>
//      log.debug(s"Received beta connection request: $msg")
      productions = productions + productionId
//      log.debug(s"Productions: ${productions.map(_.productionNameString).mkString(",")}")

      val remainingChain = fullChain.cutAt(thisConstraint)

      remainingChain.constraints match {
        case Nil => // this is the last constraint in this chain
//          log.debug(s"Alpha-Beta link to $connectMe created.")
          betaConnections.enqueue(connectMe)
          connectMe ! AlphaConnected(fullChain)

        case head :: tail => // there are more, needs a child to do the work
          val childRightForTheJob = getChildRef(alphaType, remainingChain.head)
//          log.debug(s"Passing beta connection request to $childRightForTheJob.")
          childRightForTheJob ! msg
      }

    case p@Propagate(states, _) =>
//      log.debug(s"Received propagation: $p")
      states match {
        case State(fact, StateContext(alphaChain)) :: Nil =>
          if(alphaChain.tpe != alphaType) throw AlphaPropagationException(s"Alpha node can only propagate the same type as it's own: $alphaType",p)

          // Where to the fact should be propagated
          lazy val nextNodes: List[ActorRef] = {
//            val maybeAlphaConnection = alphaChain.constraints.dropWhile(_ != thisConstraint).tail.headOption.map{ constraint => getChildRef(alphaChain.tpe, constraint)}
//            maybeAlphaConnection.toList ++ betaConnections.toList
            betaConnections.toList ++ allChildren
          }

//          log.debug(s"Binding before evaluation (${alphaType}): $fact will be applied to $thisConstraint")
          val fedConstraint = thisConstraint.bindOnAlphaInComparison(fact)
//          log.debug(s"Fed constraint: $fedConstraint")

          evalService.eval(fedConstraint::Nil) match {
            case EvalResult(Left(a)) =>
//              log.debug(s"Evaluation negative: $fact in $thisConstraint")

            case EvalResult(Right(b)) =>
              if(b) {
//                log.debug(s"Evaluation positive: $fact in $thisConstraint. "
//                + s"Sending $p to: $nextNodes")
                nextNodes.foreach{ nextNode => nextNode ! p.stateUpdated(thisConstraint)}
              } else {
                if(fact.isQuery) productions.foreach { productionId =>
//                  log.debug(s"\n\nProduction failed: $productionId")
                  reteActor ! QueryPartialResult(fact, ProductionResult.negative(productionId))
                }
//                log.debug(s"Evaluation negative: $fact in $thisConstraint")
              }
          }


        case moreFacts: List[State] => throw AlphaPropagationException(s"can't be that ${moreFacts.size} states are propagated to an AlphaActor",p)

      }
  }


}

object AlphaActor {
  def props(alphaType: FactType, constraint: Constraint, reteActor: ActorRef, evalService: EvalService): Props = {
    Props(new AlphaActor(alphaType, constraint, reteActor, evalService))
  }

  case class AlphaPropagationException(msg: String, p: Propagate) extends RuntimeException(s"Propagation problem: $msg (Propagation: $p)." )
}
