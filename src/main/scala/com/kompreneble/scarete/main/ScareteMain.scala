package com.kompreneble.scarete.main

import akka.actor.{ActorRef, ActorSystem}
import com.kompreneble.scarete.condition.{KiamaEvalService, ProductionRule}
import com.kompreneble.scarete.encoder.{AttrEncoder, FactEncoder}
import com.kompreneble.scarete.node.ReteActor
import com.kompreneble.scarete.node.ReteActor.ReteInsert
import com.kompreneble.scarete.prf.PrfUtils
import com.kompreneble.scarete.topology.FoldLeftReteTopologyService
import akka.pattern.ask
import akka.util.Timeout

import scala.concurrent.duration._
import scala.language.postfixOps

//object ScareteMain extends App {
//
//val scarete = new Scarete(ActorSystem("scarete"))
//
//}

class Scarete(actorSystem: ActorSystem) extends PrfUtils with FactEncoder {
  val prf = parsePrfRootFromResources("rules").get
  val topologyService = FoldLeftReteTopologyService
  val evalService = KiamaEvalService

  val productionRule: ProductionRule = topologyService.buildTopologyFor(prf.getRule("chocolate") :: Nil).head

  val reteActor: ActorRef = actorSystem.actorOf(ReteActor.props(productionRule::Nil, evalService), "rete")

  reteActor ! ReteInsert(Time(17, 0))
  reteActor ! ReteInsert(MilkAvailable(0.3))
  reteActor ! ReteInsert(GrainCoffeeAvailable(2))
  reteActor ! ReteInsert(SugarAvailable(2))
  reteActor ! ReteInsert(CoffeeAvailable(1))
  reteActor ! ReteInsert(CerealAvailable(1))
//
  reteActor ! ReteInsert(TemperatureOutside(10))
  reteActor ! ReteInsert(CocoaAvailable(3))
//  reteActor ! ReteInsert(MilkAvailable(0.3))
//  reteActor ! ReteInsert(GrainCoffeeAvailable(2))
//  reteActor ! ReteInsert(SugarAvailable(2))
//  reteActor ! ReteInsert(CoffeeAvailable(1))
//  reteActor ! ReteInsert(CerealAvailable(1))

  def query(i: Int) = encode(SnackQuery(i, "chocolate")).copy(isQuery = true)

  implicit val timeout = Timeout(3 seconds)
  import actorSystem.dispatcher

  for {
    i <- 1 to 1
  } {
    val res = reteActor ? ReteInsert(query(i))
    res.map { r =>
      println(r)
    }
  }


  actorSystem.scheduler.scheduleOnce(2 seconds) {
    actorSystem.terminate()
  }

}