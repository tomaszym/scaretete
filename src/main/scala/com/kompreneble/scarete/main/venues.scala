package com.kompreneble.scarete.main

case class GameVenue(id: String, category: String)

case class AssetOwner(playerId: String, venueId: String)

case class PriceQuery(playerId: String, venueId: String)
