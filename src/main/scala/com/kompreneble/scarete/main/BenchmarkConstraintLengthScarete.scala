package com.kompreneble.scarete.main

import akka.actor.{ActorRef, ActorSystem}
import akka.pattern.ask
import akka.util.Timeout
import com.kompreneble.scarete.condition.{KiamaEvalService, ProductionId, ProductionRule}
import com.kompreneble.scarete.node.ReteActor
import com.kompreneble.scarete.node.ReteActor.ReteInsert

import scala.concurrent.Await
import scala.concurrent.duration._

class BenchmarkConstraintLengthScarete(actorSystem: ActorSystem, treeHeight: Int) {


  val evalService = KiamaEvalService
  val (facts, constraintTree)= new RuleGenerator(DummyConstraintGenerator.gen).generateLeft(treeHeight)
  val (queryFact, constraintTreeWithQuery) = QueryGenerator.addQuery(constraintTree)
  val productionRule: ProductionRule = ProductionRule(constraintTreeWithQuery, ProductionId("booom!"))

  val reteActor: ActorRef = actorSystem.actorOf(ReteActor.props(productionRule::Nil, evalService), "rete")

  facts.foreach { fact =>
    reteActor ! ReteInsert(fact)
  }
  implicit val timeout = Timeout(3.seconds)
  import actorSystem.dispatcher

  def run = {
    val begin = System.nanoTime()
    val response = reteActor ? ReteInsert(queryFact)

    val f = response.map { result =>
      val end = System.nanoTime()
      Duration.fromNanos(end-begin)
    }.mapTo[Duration]

    Await.result(f, timeout.duration)
  }
}