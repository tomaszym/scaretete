package com.kompreneble.scarete.main

import akka.actor.ActorSystem
import kamon.Kamon

import scala.concurrent.Await
import scala.concurrent.duration._

object BenchmarkMain extends App {
  Kamon.start()

  val i = 1000
  val actorSystem = ActorSystem("scarete")

  val scarete = new BenchmarkConstraintLengthScarete(actorSystem, i)
  implicit val executionContext = actorSystem.dispatchers.lookup("scarete.await-dispatcher")

  actorSystem.scheduler.scheduleOnce(40.seconds) {

    val times = List.fill(20000)(1).par.map { i =>
      scarete.run.toMicros
    }
    println(i + ": " + times.sum/times.size)
  }
}
