package com.kompreneble.scarete.main

import com.kompreneble.scarete.encoder.FactEncoder

case class TemperatureOutside(celsius: Int)
case class Time(hours: Int, minutes: Int) {
  assert(hours >= 0 && hours < 24)
  assert(minutes >= 0 && minutes < 60)
}

case class MilkAvailable(volumeLiters: Double) {
  assert(volumeLiters > 0)
}

case class CoffeeAvailable(spoons: Int) {
  assert(spoons > 0)
}

case class GrainCoffeeAvailable(spoons: Int) {
  assert(spoons > 0)
}

case class CocoaAvailable(spoons: Int) {
  assert(spoons > 0)
}

case class SugarAvailable(spoons: Int) {
  assert(spoons > 0)
}

case class PowerBillPaid(isPaid: Boolean)

case class CerealAvailable(cups: Int) {
  assert(cups > 0)
}

case class SnackQuery(idx: Int, stringQuery: String)