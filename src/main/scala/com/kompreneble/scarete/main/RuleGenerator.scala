package com.kompreneble.scarete.main

import com.kompreneble.scarete.condition._
import com.kompreneble.scarete.fact.{AttrKey, AttrValue, Fact, FactType}

import scala.annotation.tailrec

class RuleGenerator(alphaGen: Int => (ConstraintChain, Fact)) {

  def generateLeft(n: Int): (List[Fact], ConstraintTree) = {
    @tailrec
    def genIter(current: ConstraintTree, facts: List[Fact], branchesLeft: Int): (List[Fact], ConstraintTree) = {
      if(branchesLeft == 0) (facts, current)
      else {
        val (nextChain, newFact) = alphaGen(branchesLeft)
        val branch = ConstraintBranch(nextChain :: current :: Nil)
        genIter(branch, newFact :: facts, branchesLeft-1)
      }
    }

    val init = alphaGen(0)
    genIter(init._1, init._2::Nil, n)
  }
}

object DummyConstraintGenerator {
  def gen(idx: Int): (ConstraintChain, Fact) = {
    val tpe = FactType(s"idx-$idx")
    val attr = AttrExpr("i")
    val attrValue = ValExpr(idx)
    val constraint = ConstraintChain(tpe, List(Constraint(Eq, attr, attrValue)))
    val fact = Fact(tpe, Map(AttrKey(attr.stringAttr) -> AttrValue(attrValue.v)))
    (constraint, fact)
  }
}

object QueryGenerator {

  def addQuery(constraints: ConstraintTree): (Fact, ConstraintTree) = {


    val factType = FactType("queryFactType")
    val queryConstraint: ConstraintChain = ConstraintChain(factType, Constraint(Eq, AttrExpr("query"), ValExpr("benchmark"))::Nil)
    val fact = Fact(factType,Map(AttrKey("query") -> AttrValue("benchmark")), isQuery = true)

    (fact, ConstraintBranch(queryConstraint :: constraints :: Nil))

  }
}