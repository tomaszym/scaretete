package topology

import com.kompreneble.scarete.condition.Constraint
import com.kompreneble.scarete.prf.ast.PrfRule
import com.kompreneble.scarete.topology.{FoldLeftReteTopologyService, ReteTopologyService}

trait TopologyUtils {

  val topologyService: ReteTopologyService = FoldLeftReteTopologyService

  def buildTopologyForTest(prfRule: PrfRule) = topologyService.buildTopologyFor(List(prfRule))

}
