package topology

import com.kompreneble.scarete.condition._
import com.kompreneble.scarete.fact.FactType
import com.kompreneble.scarete.prf.PrfUtils
import com.kompreneble.scarete.prf.ast.PrfRoot
import com.kompreneble.scarete.topology.{FoldLeftReteTopologyService, ReteTopologyService}
import org.scalatest.WordSpec

abstract class ReteTopologyServiceSpec(val reteTopologyService: ReteTopologyService) extends WordSpec with PrfUtils {

  val prfRoot: PrfRoot = parsePrfRootFromResources("milk").get
  val topology: Map[String, ProductionRule] = {
    def names = prfRoot.rules.map(_.name)
    def nodes = reteTopologyService.buildTopologyFor(prfRoot.rules)
    names.zip(nodes).toMap
  }

  "Every topology produced" should {

    "contain exactly one alpha node for every type in each rule" in {

      for {
        rule <- prfRoot.rules // in each rule
      } {
        val entitiesInRules = rule.conditions.map(_.tpe).toSet
        val alphasInTopology = {
          val list = topology(rule.name).constraintTree.constraintChains.map(_.tpe.stringType)
          val set = list.toSet
          assert(set.size == list.size, "Double alpha chain of the same type in the rule: " + rule.name)
          set
        }

        def difference: List[String] = entitiesInRules.diff(alphasInTopology).toList ++ alphasInTopology.diff(entitiesInRules)

        assert(entitiesInRules == alphasInTopology, "Alpha node types does not mirror entity types in rules: " + difference)
      }
    }
  }

}

class FoldLeftReteTopologyServiceSpec extends ReteTopologyServiceSpec(FoldLeftReteTopologyService) {

  implicit def string2factType(s: String): FactType = FactType(s)
  implicit def constraint2list(c: Constraint): List[Constraint] = List(c)


//  case class RHSStringLiteral(s: String) extends RHSExpr
//  case class RHSIntLiteral(i: Int) extends RHSExpr
//  case class RHSDecimalLiteral(d: Double) extends RHSExpr

  def eq(attr: String, s: String): Constraint = Constraint(Eq, AttrExpr(attr), ValExpr(s))
  def eq(attr: String, i: Int): Constraint = Constraint(Eq, AttrExpr(attr), ValExpr(i))
  def eq(attr: String, d: Double): Constraint = Constraint(Eq, AttrExpr(attr), ValExpr(d))

  "Topology created for hot chocolate should have the correct form" should {

    val tt = ConstraintBranch(List(
      ConstraintChain("TemperatureOutside", eq("celsius", 10)),
      ConstraintChain("Time", eq("hours", 17))
    ))

    val ttm = ConstraintBranch(List(
      tt,
      ConstraintChain("MilkAvailable", eq("volumeLiters", 0.3))
    ))

    val ttmc = ConstraintBranch(List(
      ttm,
      ConstraintChain("CocoaAvailable", eq("spoons", 3))
    ))

    val ttmcs = ConstraintBranch(List(
      ttmc,
      ConstraintChain("SugarAvailable", eq("spoons", 2))
    ))



    assert(topology("chocolate").constraintTree == ttmcs, "topologies are different")

  }

}