package rules

import com.kompreneble.scarete.prf.{PrfParsers, PrfUtils}
import com.kompreneble.scarete.prf.ast._
import org.scalatest.FlatSpec
import shapeless.ops.nat.Prod



class ParserSpec extends FlatSpec with PrfUtils {

  val seriousExample = """rule "CenaBazowa" if:
                         | in PriceQuery(
                         |   routeId = "KrakowRzeszow",
                         |   seatNo = 10 + (((1 + 2 * (5 - 1)))),
                         |   passengerId = $passengerId,
                         |   discount = 0.1234,
                         |   month(date) = 12
                         | ) in Passenger(id = $passengerId)
                         |  then (
                         |   TODO
                         | )
                         |""".stripMargin

  val parsers = new PrfParsers

  "Parser" should "parse tokens to AST from a serious example" in {

    val res = parsers.parseRootString(seriousExample).get

    assert(res.rules.head === PrfRule(
      "CenaBazowa",
      List(
        PrfEntityConditions(tpe = "PriceQuery", List(
          PrfArithmeticCondition("=",
            PrfAttribute("routeId"), PrfStringLiteral("KrakowRzeszow")),
          PrfArithmeticCondition("=", PrfAttribute("seatNo"), PrfSum(List(
            PrfSumElem(PrfPlus, 10),
            PrfSumElem(PrfPlus, PrfSum(List(
              PrfSumElem(PrfPlus, 1),
              PrfSumElem(PrfPlus, PrfProd(List(
                PrfProdElem(PrfMul, 2),
                PrfProdElem(PrfMul, PrfSum(List(
                  PrfSumElem(PrfPlus, 5),
                  PrfSumElem(PrfMinus, 1)
                )))
              ))))))))),
          PrfArithmeticCondition("=",
            PrfAttribute("passengerId"),
            PrfBinding("passengerId")),
          PrfArithmeticCondition("=",
            PrfAttribute("discount"),
            PrfDecimalLiteral(0.1234)),
          PrfArithmeticCondition("=",
            PrfFun("month", PrfAttribute("date"))
            , 12)
        )),
        PrfEntityConditions(tpe = "Passenger", List(
          PrfArithmeticCondition("=",
            PrfAttribute("id"),
            PrfBinding("passengerId"))
        ))
      ),
      PrfEffect("TODO")))
  }
  val prodInSumExample = """1 + 2 * (5 - 1)""".stripMargin

  "Parser" should "parse tokens to AST from a prod in sum example" in {

    val res = parsers.parseSumString(prodInSumExample).get

    assert(res ===
      PrfSum(List(
        PrfSumElem(PrfPlus, 1),
        PrfSumElem(PrfPlus, PrfProd(List(
          PrfProdElem(PrfMul, 2),
          PrfProdElem(PrfMul, PrfSum(List(
            PrfSumElem(PrfPlus, intLiteral(5)),
            PrfSumElem(PrfMinus, 1)
          )))
        )))
      ))
    )
  }


  val doubleBindingProdExample = """5 * 2 + 2 * 2""".stripMargin
  "Parser" should "parse tokens to AST from a proddoubleBindingProdExample" in {

    val res = parsers.parseSumString(doubleBindingProdExample).get

    def xs(n: Int) = PrfProd(List(PrfProdElem(PrfMul ,n), PrfProdElem(PrfMul, PrfIntLiteral(2))))
    assert(res ===
      PrfSum(List(
        PrfSumElem(PrfPlus, xs(5)),
        PrfSumElem(PrfPlus, xs(2))
      ))
    )
  }

 }
