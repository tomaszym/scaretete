package models

import com.kompreneble.scarete.fact.{AttrKey, AttrValue, Fact, FactType}


object prices {

  case class PriceQuery(routeId: String, date: String) {
    def toFact = Fact(tpe = FactType("PriceQuery"), attributes = Map(
      AttrKey("routeId") -> AttrValue(routeId),
      AttrKey("date") -> AttrValue(date)
    ))
  }

  case class Today(date: String) {

    def toFact = Fact(tpe = FactType("Today"), attributes = Map(
      AttrKey("date") -> AttrValue(date)
    ))
  }

  case class SpecialOfferPeriod(from: String, to: String) {

    def toFact = Fact(tpe = FactType("SpecialOfferPeriod"), attributes = Map(
      AttrKey("from") -> AttrValue(from),
      AttrKey("to") -> AttrValue(to)
    ))
  }

}
