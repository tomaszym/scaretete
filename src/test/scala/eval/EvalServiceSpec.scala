package eval

import com.kompreneble.scarete.condition.KiamaEvalService.ExtractX
import com.kompreneble.scarete.condition._
import com.kompreneble.scarete.prf.PrfUtils
import org.scalatest.{Matchers, WordSpec}
import topology.TopologyUtils

trait TestImplicits {

  implicit def string2expr(s: String) = ValExpr(s)
  implicit def long2expr(l: Long) = ValExpr(l)
  implicit def double2expr(d: Double) = ValExpr(d)

}

abstract class EvalServiceSpec(evalService: EvalService) extends WordSpec with TestImplicits with Matchers {

//  "Eval service" should {
//
//    "Correctly eval when no simplification is needed" in {
//      assert(evalService.eval(Comparison(Eq,"a", "a") :: Nil) === EvalResult.tru)
//      assert(evalService.eval(Comparison(Eq,"a", "b") :: Nil) === EvalResult.fals)
//
//      assert(evalService.eval(Comparison(Eq,6l, 6l) :: Nil) === EvalResult.tru)
//      assert(evalService.eval(Comparison(Eq,1l, 4l) :: Nil) === EvalResult.fals)
//
//      assert(evalService.eval(Comparison(Eq,5d, 5d) :: Nil) === EvalResult.tru)
//      assert(evalService.eval(Comparison(Eq,1d, 10d) :: Nil) === EvalResult.fals)
//    }
//
//    "Throw exceptions when string is being compared to a number" in {
//
//      an [EvalException] should be thrownBy {
//        evalService.eval(Comparison(Eq,0l, "0") :: Nil)
//      }
//      an [EvalException] should be thrownBy {
//        evalService.eval(Comparison(Eq,0l, 0d) :: Nil)
//      }
//      an [EvalException] should be thrownBy {
//        evalService.eval(Comparison(Eq,0d, "0") :: Nil)
//      }
//    }
//  }
}

class NoEvalServiceSpec extends EvalServiceSpec(NoEvalService)
class KiamaEvalServiceSpec extends EvalServiceSpec(KiamaEvalService) with PrfUtils with TopologyUtils {

  implicit def stringRule(stringRule: String): List[Constraint] = buildTopologyForTest(parseRule(stringRule)).flatMap(_.constraintTree.constraintChains.flatMap(_.constraints))

//  "Extract x from a comparison" in {
//
//    val res = SumExpr(List(VariableBindingExpr("x"), ValExpr(5))) match {
//      case ExtractX(x, rest) => Some((x, rest))
//      case els: SumExpr => None
//    }
//    assert(res === Some((VariableBindingExpr("x"), SumExpr(ValExpr(5)::Nil))))
//  }


  "Do not make things worse when not needed" in {
    assert(KiamaEvalService.eval(
      """
        |rule "Foo" if:
        |  in A(1 = 1)
        |then (TODO)
      """.stripMargin) === EvalResult.tru)
  }

  "Do a simple substitution in n-th comparision" in {
    assert(KiamaEvalService.eval("""
                         rule "CenaBazowa" if:
                            | in PriceQuery("KrakowRzeszow" = "KrakowRzeszow", 15 = 15, "Alice" = $passengerId, 0.1234 = 0.1234)
                            | in Passenger("Alice" = $passengerId)
                            |  then (
                            |   TODO
                            | )
                            |
                            | """.stripMargin) === EvalResult.tru)
  }

  "Normalize Plus/MinusExpr" in {
    assert(KiamaEvalService.eval(
      """rule "Test" if:
        | in A( 10 + 12 = 15 + 7)
        | then (cool)
      """.stripMargin) === EvalResult.tru
    )
  }

  "Normalize Plus/MinusExpr with susbtitution" in {
    assert(KiamaEvalService.eval(
      """rule "CenaBazowa" if: in A(
        |   $x + 5 = 2 + 8,
        |       $x = 5)
        | then (TODO)""".stripMargin) === EvalResult(Right(true)), "x + 5 = 10")

    assert(KiamaEvalService.eval(
      """rule "CenaBazowa" if: in A(
        |   5 + $x = 1000,
        |       $x = 5)
        | then (TODO)""".stripMargin) === EvalResult(Right(false)), "5 + x = 10")

    assert(KiamaEvalService.eval(
      """rule "CenaBazowa" if: in A(
        |   10 = $x + 5,
        |   $x = 5)
        | then (TODO)""".stripMargin) === EvalResult(Right(true)), "10 = x + 5")

    assert(KiamaEvalService.eval(
      """rule "CenaBazowa" if: in A(
        |   10 = 5 + $x,
        |   $x = 5)
        | then (TODO)""".stripMargin) === EvalResult(Right(true)), "10 = 5 + x")
  }

  "Normalize three equations" in {
    assert(KiamaEvalService.eval(
      """rule "CenaBazowa" if: in A(
        |   $x + $y = 17 - $z,
        |   $y - $x = 3 + $z,
        |   $x = $z + 3 )
        | then (nothing)""".stripMargin) === EvalResult.tru, "x = 5, y = 10, z = 2")
  }

//  "Do a double substitution comparision and then one single with the resulti" ignore {
//    assert(KiamaEvalService.eval(
//      """
//                         rule "CenaBazowa" if:
//        | in A()
//        | in B()
//        |  then (
//        |   TODO
//        | )
//        |
//        | """.stripMargin) === EvalResult(Right(true)))
//  }
}