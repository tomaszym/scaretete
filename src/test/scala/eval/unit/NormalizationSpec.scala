package eval.unit

import _root_.eval.TestImplicits
import com.kompreneble.scarete.condition.KiamaEvalService._
import com.kompreneble.scarete.condition._
import com.kompreneble.scarete.condition.eval.canonical.CanonicalPolynomialRules
import com.kompreneble.scarete.condition.eval.normal.NormalRules
import com.kompreneble.scarete.prf.PrfUtils
import org.kiama.rewriting.Rewriter._
import org.scalatest.{Matchers, WordSpec}
import topology.TopologyUtils

class NormalizationSpec extends WordSpec with TestImplicits with Matchers with PrfUtils with TopologyUtils with NormalRules {

  implicit def stringRule(stringRule: String): List[Constraint] = {
    val betaConstraint: List[ConstraintTree] = buildTopologyForTest(parseRule(stringRule)).map(_.constraintTree)
    //we are not interested in the structure of nodes, only flat constraints:
    val flatComparisions: List[Constraint] = betaConstraint.flatMap(_.constraintChains.flatMap(_.constraints))
    // and here we have set of comparisions to solve
    flatComparisions

  }

  def doTest(comparisons: List[Constraint], expected: Constraint): Unit = {
    assert(comparisons.size == 1)// this test is interested in handling single-comparision relation
    val underTest = comparisons.head
//
//    def normalize(e:Expr) = normalizeStrategy(e) match {
//      case Some(transformed: Expr) => transformed
//      case Some(els) => throw new IllegalStateException(s"got $els after applying normalization to $e")
//      case None => e // no rule applied, return the old value
//    }

    val normalized: Relations = reduce(normalizeStrategy)(Relations(comparisons)).map(_.asInstanceOf[Relations]).get

    assert(normalized.cs.head === expected)
  }

  "RichExprList" should {
    val onlyNumbers = List(ValExpr(5), ValExpr(2.5), ValExpr(12353))
    "Tell whether List[Expr] contains only numbers" in {
      assert(onlyNumbers.allNumeric, "all numeric")
      assert(!(ValExpr("asdf") :: onlyNumbers).allNumeric, "with a string")
      assert(!(SumExpr(ValExpr(5) :: Nil) :: onlyNumbers).allNumeric, "numeric with subsum")
    }

    "Could tell wheter List[Expr] contains any zeros and filter them" in {
      assert(!onlyNumbers.containsZeros)
      assert((ValExpr(0)::onlyNumbers).containsZeros)
      assert((ValExpr(0)::onlyNumbers).filterNonZeros === onlyNumbers)
    }
  }


  "Normalization Rules" should {

    "Handle trivial cases 7=7 => 7=7" in {
      doTest("""rule "CenaBazowa" if:
               | in PriceQuery(7 = 7)
               |  then (TODO)""".stripMargin, Constraint(Eq, ValExpr(7), ValExpr(7)))
    }

    "Transform numbers only 5+2=10-3 => 7=7" in {
      doTest("""rule "CenaBazowa" if:
               | in PriceQuery(5 + 2 = 10 - 3)
               |  then (TODO)""".stripMargin, Constraint(Eq, ValExpr(7), ValExpr(7)))
    }

    "handle serious expressions" in {
      doTest("""rule "CenaBazowa" if:
               | in PriceQuery($z * $z / 1 / $z / 1 / $z = 1)
               |  then (TODO)""".stripMargin, Constraint(Eq, ValExpr(1), ValExpr(1)))
    }

  }

}
