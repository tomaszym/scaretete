//package eval.unit
//
//import com.kompreneble.scarete.condition._
//import _root_.eval.TestImplicits
//import com.kompreneble.scarete.condition.eval.canonical.CanonicalPolynomialRules
//import org.scalatest.{Matchers, WordSpec}
//import com.kompreneble.scarete.prf.PrfUtils
//import topology.TopologyUtils
//
//class CanonicalPolynomialSpec extends WordSpec with TestImplicits with Matchers with PrfUtils with TopologyUtils with CanonicalPolynomialRules {
//
//  implicit def stringRule(stringRule: String): List[Constraint] = {
//    val betaConstraint: List[ConstraintTree] = buildTopologyForTest(parseRule(stringRule))
//    //we are not interested in the structure of nodes, only flat constraints:
//    val flatComparisions: List[Constraint] = betaConstraint.flatMap(_.constraintChains.flatMap(_.constraints))
//    // and here we have set of comparisions to solve
//    flatComparisions
//
//  }
//
//  def doTest(comparisons: List[Constraint], expected: Constraint): Unit = {
//    assert(comparisons.size == 1)// this test is interested in handling single-comparision relation
//    val underTest = Relations(comparisons)
//
//
////    assert(canonicalize(underTest) === Relations(expected::Nil))
////
//  }
//
//  "CanonicalPolynomialRules" should {
//
//    "Handle trivial cases 7=7 => 7=7" ignore {
//      doTest("""rule "CenaBazowa" if:
//               | in PriceQuery(7 = 7)
//               |  then (TODO)""".stripMargin, Constraint(Eq, ValExpr(7), ValExpr(7)))
//    }
//
//    "Transform numbers only 5+2=10-3 => 7=7" ignore {
//      doTest("""rule "CenaBazowa" if:
//               | in PriceQuery(5 + 2 = 10 - 3)
//               |  then (TODO)""".stripMargin, Constraint(Eq, ValExpr(7), ValExpr(7)))
//    }
//    "Transform bindings only" ignore {
//      doTest("""rule "CenaBazowa" if:
//               | in PriceQuery(2 * $number + 2 * $number = 4 * $number)
//               |  then (TODO)""".stripMargin, Constraint(Eq, ValExpr(0), ValExpr(0)))
//    }
//  }
//
//}
