//package eval.unit
//
//import com.kompreneble.scarete.condition.eval.canonical.CanonicalFormSumReductionStrategy
//import com.kompreneble.scarete.condition.{ProdExpr, SumExpr, ValExpr, VariableBindingExpr}
//import eval.TestImplicits
//import org.scalatest.{Matchers, WordSpec}
//import com.kompreneble.scarete.prf.PrfUtils
//import topology.TopologyUtils
//import org.kiama.rewriting.Rewriter._
//
//class SumReductionEvalSpec extends WordSpec with TestImplicits with Matchers with PrfUtils with TopologyUtils{
//
//
//  def five = ValExpr(5l)
//  def half = ValExpr(0.5)
//  def maljon = ValExpr(100000000000000000l)
//
//  def testVals: List[ValExpr] = List(five,half,maljon)
//
//  "SumReductionEval" should {
//
//    "Be able to sum when there are no variable bindings" in new CanonicalFormSumReductionStrategy {
//
//      assert(reduceSums(SumExpr(List(ValExpr(5)),List(ValExpr(3)))) === ValExpr(2))
//
//      for {
//        plusX <- testVals
//        minusX <- testVals
//        plusY <- testVals
//        minusY <- testVals
//      } {
//        val res = reduceSums(SumExpr(List(plusX,plusY), List(minusX,minusY)))
//        res shouldBe a [ValExpr]
//      }
//    }
//
//    "Be able to sum when there are only ProdExprs (no need to wrap in prod eg. x = 1*x)" in new CanonicalFormSumReductionStrategy {
//
//      val prodA = ProdExpr(ValExpr(4)::VariableBindingExpr("date")::Nil)
//      val prodB = ProdExpr(ValExpr(2)::VariableBindingExpr("date")::Nil)
//      val prodC = ProdExpr(ValExpr(6)::VariableBindingExpr("date")::Nil)
//
//      val result = reduce(sumReductionStrategy).apply(SumExpr(prodA::prodB::Nil))
//      assert(result === Some(SumExpr(prodC::Nil)))
//    }
//
//    "Be able to sum when there is need to wrap Expr in prod (eg. x = 1*x)" in new CanonicalFormSumReductionStrategy {
//
//      val a = VariableBindingExpr("date")
//      val b = ProdExpr(ValExpr(2)::VariableBindingExpr("date")::Nil)
//      val c = ProdExpr(ValExpr(3)::VariableBindingExpr("date")::Nil)
//
//      assert(reduce(sumReductionStrategy).apply(SumExpr(a :: b ::Nil)) === Some(SumExpr(c::Nil)))
//    }
//
//
//  }
//
//
//
//
//}
