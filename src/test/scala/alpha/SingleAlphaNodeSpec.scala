package alpha

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import com.kompreneble.scarete.condition._
import com.kompreneble.scarete.fact._
import com.kompreneble.scarete.node.{AlphaActor, State, StateContext}
import com.kompreneble.scarete.node.BetaActor.{AlphaConnected, ConnectAlpha}
import com.kompreneble.scarete.node.ReteActor.Propagate
import com.kompreneble.scarete.prf.PrfUtils
import com.kompreneble.scarete.topology.FoldLeftReteTopologyService
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}


class SingleAlphaNodeSpec(_system: ActorSystem) extends TestKit(_system) with ImplicitSender with WordSpecLike with BeforeAndAfterAll with PrfUtils {
  def this() = this(ActorSystem("MySpec"))

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  val prf = parsePrfRootFromResources("krk-rze").get
  val topologyService = FoldLeftReteTopologyService
  val evalService = NoEvalService


  "One, signle-constraint alpha node" should {
    val factType = FactType("PriceQuery")

    val alphaChain: ConstraintChain = topologyService.buildTopologyFor(prf.getRule("specific-routeId") :: Nil).head.constraintTree match {
      case a@ConstraintChain(tpe, constraint :: Nil) => a
      case _: ConstraintTree => throw new RuntimeException("specific-routeId needs to have constraint only for this test")
    }

    val reteActorProbe = TestProbe()
    val alphaActor = _system.actorOf(AlphaActor.props(alphaChain.tpe,alphaChain.constraints.head, reteActorProbe.ref, evalService))
    "let betas connect to it" in {

      alphaActor ! ConnectAlpha(alphaChain, ProductionId("dummyProduction"), self)

      expectMsg(AlphaConnected(alphaChain))
    }

    "propagate Propagation if passes test" in {
      val fact = Fact(factType,Map(AttrKey("routeId") -> AttrValue("KrakowRzeszow")))
      val propagateMsg = Propagate(
        State.empty(fact) :: Nil
      )
      alphaActor ! propagateMsg

      expectMsg(Propagate(State(fact, StateContext(alphaChain))::Nil))
    }

    "not propagate if test fails" in {
      val propagateMsg = Propagate(
        State(Fact(factType,Map(AttrKey("routeId") -> AttrValue("OtherThanKrakowRzeszow"))), StateContext(alphaChain)) :: Nil
      )
      alphaActor ! propagateMsg

      expectNoMsg()
    }
  }


  "One, two-constraint alpha node" should {
    val factType = FactType("PriceQuery")

    val alphaChain: ConstraintChain = topologyService.buildTopologyFor(prf.getRule("specific-routeId-and-seatNo") :: Nil).head.constraintTree match {
      case a@ConstraintChain(tpe, constraintA :: constraintB :: Nil) => a
      case _: ConstraintTree => throw new RuntimeException("specific-routeId-and-seatNo needs to have exactly two constraints for this test")
    }


    val reteActorProbe = TestProbe()
    val alphaActor = _system.actorOf(AlphaActor.props(alphaChain.tpe,alphaChain.constraints.head, reteActorProbe.ref, evalService))
    "let betas connect to it" in {

      alphaActor ! ConnectAlpha(alphaChain, ProductionId("dummyId"), self)

      expectMsg(AlphaConnected(alphaChain))
    }

    "propagate Propagation if passes test" in {
      val state = State.empty(Fact(factType,Map(
        AttrKey("routeId") -> AttrValue("KrakowRzeszow"),
        AttrKey("seatNo") -> AttrValue(15)
      )))

      alphaActor ! Propagate(state::Nil)

      expectMsg(Propagate(state.copy(stateContext = StateContext(alphaChain))::Nil))
    }

    "not propagate if test fails" in {
      val propagateMsg = Propagate(
        State(Fact(factType,Map(AttrKey("routeId") -> AttrValue("OtherThanKrakowRzeszow"),AttrKey("seatNo") -> AttrValue(15000))), StateContext(alphaChain)) :: Nil
      )
      alphaActor ! propagateMsg

      expectNoMsg()
    }

    "not propagate if no such attribute" in {
      val propagateMsg = Propagate(
        State(Fact(factType,Map(
          AttrKey("routeId") -> AttrValue("OtherThanKrakowRzeszow")
        )), StateContext(alphaChain)) :: Nil
      )
      alphaActor ! propagateMsg

      expectNoMsg()
    }
  }
//

}
