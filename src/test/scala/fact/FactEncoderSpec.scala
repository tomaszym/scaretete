package fact

import com.kompreneble.scarete.encoder.{FactEncoder, RootFactEncoder}
import com.kompreneble.scarete.fact._
import org.scalatest.WordSpec

case class EmptyCaseClass()
case class StringCaseClass(s: String)
case class NestedStringCaseClass(stringCaseClass: StringCaseClass)

class FactEncoderSpec extends WordSpec {


  "Fact objects" should  {
    "be encoded to RootFact when empty case class" in new FactEncoder {
      val empty: EmptyCaseClass = EmptyCaseClass()
      val encoder = implicitly[RootFactEncoder[EmptyCaseClass]]
      assert(encoder.encode(empty) === Fact(FactType(empty.getClass.getName.split('.').last), Map(), false))
    }
    "be encoded to RootFact when single string case class" in new FactEncoder {
      val s = StringCaseClass(":E")
      val encoder = implicitly[RootFactEncoder[StringCaseClass]]
      assert(encoder.encode(s) === Fact(FactType(s.getClass.getName.split('.').last), Map(AttrKey("s") -> AttrValue(":E")), false))
    }
    "be encoded to RootFact when nested case class with string" in new FactEncoder {
      val nested = NestedStringCaseClass(StringCaseClass(":E:E"))
      val encoder = implicitly[RootFactEncoder[NestedStringCaseClass]]
      assert(encoder.encode(nested) === Fact(FactType(nested.getClass.getName.split('.').last), Map(
        AttrKey("stringCaseClass") -> SubFact(FactType("StringCaseClass"), Map(
          AttrKey("s") -> AttrValue(":E:E")
        ))
      ), false))
    }


  }

}