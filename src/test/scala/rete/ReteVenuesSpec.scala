package rete

import akka.actor.{ActorRef, ActorSystem}
import akka.testkit.{ImplicitSender, TestKit}
import com.kompreneble.scarete.condition.{KiamaEvalService, ProductionId}
import com.kompreneble.scarete.encoder.FactEncoder
import com.kompreneble.scarete.fact.{AttrKey, AttrValue, Fact, FactType}
import com.kompreneble.scarete.main._
import com.kompreneble.scarete.node.ReteActor.ReteInsert
import com.kompreneble.scarete.node.{QueryResult, ReteActor}
import com.kompreneble.scarete.prf.PrfUtils
import com.kompreneble.scarete.topology.FoldLeftReteTopologyService
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

class ReteVenuesSpec(_system: ActorSystem) extends TestKit(_system) with ImplicitSender with WordSpecLike with BeforeAndAfterAll with PrfUtils  with FactEncoder {
  def this() = this(ActorSystem("MySpec"))

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  val prf = parsePrfRootFromResources("venues").get

  val topologyService = FoldLeftReteTopologyService
  val evalService = KiamaEvalService

  val productionRules = topologyService.buildTopologyFor(prf.rules)
  println(productionRules)
  val reteActor: ActorRef = system.actorOf(ReteActor.props(productionRules, evalService), "rete")

  "Venue example" should {

    val hotelVenue = ReteInsert(GameVenue("1", "Hotel"))
    val skiChairLiftVenue = ReteInsert(GameVenue("2", "SkiChairLift"))
    val assets = ReteInsert(AssetOwner("alice", venueId = "1")) :: ReteInsert(AssetOwner("alice", venueId = "2")) :: Nil
    val inserts = hotelVenue :: skiChairLiftVenue :: assets

    inserts.foreach { f => reteActor ! f }

    _system.scheduler.scheduleOnce(3000.millis) {
      val query = PriceQuery("alice", "1")
      reteActor ! ReteInsert(encode(query).copy(isQuery = true))
    }



    expectMsg(5.seconds, QueryResult(
      on = Set(ProductionId("HotelSkiBonus"), ProductionId("HotelBonus")),
      off = Set(ProductionId("ParkBonus")))
    )


    assert(true)
  }


}
