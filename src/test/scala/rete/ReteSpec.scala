package rete

import akka.actor.{ActorRef, ActorSystem}
import akka.testkit.{ImplicitSender, TestKit}
import com.kompreneble.scarete.condition.{ConstraintTree, KiamaEvalService, ProductionId}
import com.kompreneble.scarete.fact.{AttrKey, AttrValue, Fact, FactType}
import com.kompreneble.scarete.node.{QueryResult, ReteActor}
import com.kompreneble.scarete.node.ReteActor.ReteInsert
import com.kompreneble.scarete.topology.FoldLeftReteTopologyService
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}
import akka.pattern.ask
import com.kompreneble.scarete.prf.PrfUtils


class ReteSpec(_system: ActorSystem) extends TestKit(_system) with ImplicitSender with WordSpecLike with BeforeAndAfterAll with PrfUtils {
  def this() = this(ActorSystem("MySpec"))

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  val prf = parsePrfRootFromResources("krk-rze").get
  val topologyService = FoldLeftReteTopologyService
  val evalService = KiamaEvalService

  val productionRule = topologyService.buildTopologyFor(prf.getRule("specific-routeId-and-passengerId") :: Nil).head

  val reteActor: ActorRef = system.actorOf(ReteActor.props(productionRule::Nil, evalService), "rete")

  "One, two-constraint alpha node" should {
    val priceQueryFact = Fact(FactType("PriceQuery"), Map(AttrKey("routeId") -> AttrValue("KrakowRzeszow")), isQuery = true)
    val passengerIdFact = Fact(FactType("Passenger"), Map(AttrKey("id") -> AttrValue("1234")))

    val p = ReteInsert(priceQueryFact)
    val q = ReteInsert(passengerIdFact)

    reteActor ! q
    Thread.sleep(100)
    reteActor ! p

    expectMsg(QueryResult(Set(ProductionId("TODO")), Set()))


    assert(true)
  }


}
