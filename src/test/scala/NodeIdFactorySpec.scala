//import com.kompreneble.scarete.common.{NodeIdFactory, NodeId}
//import com.kompreneble.scarete.condition.AlphaChain
//import com.kompreneble.scarete.prf.PrfParsers
//import com.kompreneble.scarete.topology.RuleTreeFactory
//import org.scalatest.WordSpec
//
//class NodeIdFactorySpec extends WordSpec {
//
//  val parsers = new PrfParsers
//
//  implicit def string2constraint(prfString: String): AlphaChain = {
//    val prf = parsers.parseString(parsers.parseEntityConditions, prf).get
//    RuleTreeFactory.buildCondition(prf)
//  }
//
//  implicit def string2nodeId(stringId: String): NodeId = NodeId(stringId)
//
//  def test(ec: AlphaChain, correct: NodeId) = {
//
//
//    val computed = NodeIdFactory.computeAlphaId(ec)
//
//    assert(computed == correct)
//  }
//
//  "IdFactory" when {
//    "composing id for AlphaNode with only containing attribute constraints (no evaluating on LHS)" must {
//      "get it right for one-field constraint " in {
//
//        test("""PriceQuery(routeId = "KrkRze")""", "asd")
//
//      }
//      "something else" in {
//        assert(true)
//      }
//    }
//  }
//
//
//}
